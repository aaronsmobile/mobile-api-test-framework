﻿using AutoFrameWork.Preparation;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API_Test_Framework.Tests
{
    [SetUpFixture]
    public class GroupSetup
    {
      
        [OneTimeSetUp]
        public void RunBeforeAnyTests()
        {
            AutoFrameWork.Core.Settings.CreateVideo = false;
            AutoFrameWork.Core.Settings.CreateScreenshot = false;
            AutoFrameWork.Core.Settings.SendStatsEmail = false;
            AutoFrameWork.Core.Settings.EmailRecipients = "reginald.graham@aarons.com";
            Prep.OneTimeSetup(new StackTrace().GetFrame(1).GetMethod());
        }

        [OneTimeTearDown]
        public void RunAfterAnyTests()
        {
            Prep.OneTimeTearDown("Mobile API Regression Suite Test Execution: ", "Mobile API Regression Suite Test Execution: ");
        }

    }
}
