﻿using AutoFrameWork.Core;
using RestSharp;
using System;
using System.IO;

namespace API_Test_Framework
{
    internal class GetAuth0LoginResponse
    {
        static GetAuth0LoginResponse()
        {
        }

        public string GetAccessToken()
        {
            RestClient client = new RestClient(Settings.Setting.NonProdEnvironments.OktaUrl.SupplyValue());
            RestRequest request = new RestRequest("/oauth2/default/v1/token", Method.POST);

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("application/x-www-form-urlencoded", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\LoginRequest.txt"), ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<Auth0Login.Login>(request);
            var message = response.Data.access_token;

            string accessToken = message.ToString();

            return accessToken;
        }

        public string GetSpecificUserAccessToken(string userName, string password)
        {
            RestClient client = new RestClient(Settings.Setting.NonProdEnvironments.OktaUrl.SupplyValue());
            RestRequest request = new RestRequest("/oauth2/default/v1/token", Method.POST);

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddHeader("Accept", "application/json");
            request.AddParameter("grant_type", "password");
            request.AddParameter("client_id", "0oafn40cafMYU7gcc0h7");
            request.AddParameter("scope", "openid email offline_access");
            request.AddParameter("username", userName);
            request.AddParameter("password", password);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<Auth0Login.Login>(request);
            var message = response.Data.access_token;

            string accessToken = message.ToString();

            return accessToken;
        }

        public string GetManagementAccessToken()
        {
            RestClient client = new RestClient(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue());
            RestRequest request = new RestRequest("/oauth2/default/v1/token", Method.POST);

            request.AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\GetManagementToken.txt"), ParameterType.RequestBody);
            request.RequestFormat = DataFormat.Json;

            var response = client.Execute<Auth0Login.Login>(request);
            var message = response.Data.access_token;

            string accessToken = message.ToString();

            return accessToken;
        }

        public class Auth0Login
        {
            public Auth0Login()
            {
            }

            public class Login
            {
                public string access_token { get; set; }
                public string refresh_token { get; set; }
                public string id_token { get; set; }
                public string scope { get; set; }
                public int expires_in { get; set; }
                public string token_type { get; set; }
            }
        }
    }
}