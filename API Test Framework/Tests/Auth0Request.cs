﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using AutoFrameWork.Extensions;
using NUnit.Framework;
using System;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Auth0 Authentication"), Harness("API")]
    //public class Mobile API Auth0:Prep
    public class Auth0Request
    {
        #region Preparation

        /*[OneTimeSetUp]
        public void OneTimeSetup()
        {
            //GetAuth0LoginResponse getAuth0LoginResponseObject = new GetAuth0LoginResponse();
            //getAuth0LoginResponseObject.GetAccessToken();
        }

        [SetUp]
        public new void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }*/

        #endregion Preparation

        //[Test, Description("Auth0 Login"), Author("Reginald Graham")]
        public void Auth0LoginRequest()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0LoginRequest", Is.LessThanOrEqualTo(2500));
                Step<Auth0RequestUrl>.Time("Auth0LoginRequest", Is.LessThanOrEqualTo(15));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0LoginRequest", Is.EqualTo(200));
                Step<Auth0RequestUrl>.ApiJson("Auth0LoginRequest", "access_token", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0LoginRequest", "refresh_token", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0LoginRequest", "id_token", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0LoginRequest", "scope", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0LoginRequest", "expires_in", Is.All.InstanceOf<Int64>());
                Step<Auth0RequestUrl>.ApiJson("Auth0LoginRequest", "token_type", Is.All.InstanceOf<string>());
            });
        }

        //[Test, Description("Auth0 Sign Up"), Author("Reginald Graham")]
        public void Auth0SignUpRequestReturns400ForExistingUser()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0SignUpRequest", Is.LessThanOrEqualTo(500));
                Step<Auth0RequestUrl>.Time("Auth0SignUpRequest", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0SignUpRequest", Is.EqualTo(400));
            });
        }

        //[Test, Description("Auth0 Search Users"), Author("Reginald Graham")]
        public void Auth0SearchUsersWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0SearchUsersWithToken", Is.LessThanOrEqualTo(1000));
                Step<Auth0RequestUrl>.Time("Auth0SearchUsersWithToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0SearchUsersWithToken", Is.EqualTo(200));
                Step<Auth0RequestUrl>.ApiJson("Auth0SearchUsersWithToken", ".[*].email_verified", Is.All.InstanceOf<bool>());
                Step<Auth0RequestUrl>.ApiJson("Auth0SearchUsersWithToken", ".[*].user_id", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0SearchUsersWithToken", ".[*].user_metadata.GlobalCustomerId", Is.All.InstanceOf<string>());
            });
        }

        //[Test, Description("Auth0 Search Users"), Author("Reginald Graham")]
        public void Auth0SearchUsersWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0SearchUsersWithoutToken", Is.LessThanOrEqualTo(1000));
                Step<Auth0RequestUrl>.Time("Auth0SearchUsersWithoutToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0SearchUsersWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Auth0 Search Users"), Author("Reginald Graham")]
        public void Auth0SearchUsersExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0SearchUsersExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<Auth0RequestUrl>.Time("Auth0SearchUsersExpiredToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0SearchUsersExpiredToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Auth0 Update User"), Author("Reginald Graham")]
        public void Auth0UpdateUserWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0UpdateUserWithToken", Is.LessThanOrEqualTo(700));
                Step<Auth0RequestUrl>.Time("Auth0UpdateUserWithToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0UpdateUserWithToken", Is.EqualTo(200));
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "email_verified", Is.All.InstanceOf<bool>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "email", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "updated_at", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "name", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "picture", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "user_id", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "nickname", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "identities.[*].user_id", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "identities.[*].provider", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "identities.[*].connection", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "identities.[*].isSocial", Is.All.InstanceOf<bool>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "created_at", Is.All.TimeFormat24Hour());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "user_metadata.GlobalCustomerId", Is.All.InstanceOf<Int64>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "last_ip", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "last_login", Is.All.TimeFormat24Hour());
                Step<Auth0RequestUrl>.ApiJson("Auth0UpdateUserWithToken", "logins_count", Is.All.InstanceOf<Int64>());
            });
        }

        //[Test, Description("Auth0 Update User"), Author("Reginald Graham")]
        public void Auth0UpdateUserWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0UpdateUserWithoutToken", Is.LessThanOrEqualTo(700));
                Step<Auth0RequestUrl>.Time("Auth0UpdateUserWithoutToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0UpdateUserWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Auth0 Update User"), Author("Reginald Graham")]
        public void Auth0UpdateUserExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0UpdateUserExpiredToken", Is.LessThanOrEqualTo(700));
                Step<Auth0RequestUrl>.Time("Auth0UpdateUserExpiredToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0UpdateUserExpiredToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Auth0 Get Management Token"), Author("Reginald Graham")]
        public void Auth0GetManagementTokenWithAccessToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0GetManagementAPIWithToken", Is.LessThanOrEqualTo(5000));
                Step<Auth0RequestUrl>.Time("Auth0GetManagementAPIWithToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0GetManagementAPIWithToken", Is.EqualTo(200));
                Step<Auth0RequestUrl>.ApiJson("Auth0GetManagementAPIWithToken", "access_token", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0GetManagementAPIWithToken", "scope", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0GetManagementAPIWithToken", "expires_in", Is.All.InstanceOf<Int64>());
                Step<Auth0RequestUrl>.ApiJson("Auth0GetManagementAPIWithToken", "token_type", Is.All.InstanceOf<string>());
            });
        }

        //[Test, Description("Auth0 Refresh Token"), Author("Reginald Graham")]
        public void Auth0RefreshTokenWithAccessToken()
        {
            Assert.Multiple(() =>
            {
                Step<Auth0RequestUrl>.FileSize("Auth0RefreshWithToken", Is.LessThanOrEqualTo(2500));
                Step<Auth0RequestUrl>.Time("Auth0RefreshWithToken", Is.LessThanOrEqualTo(1));
                Step<Auth0RequestUrl>.StatusCode<int>("Auth0RefreshWithToken", Is.EqualTo(200));
                Step<Auth0RequestUrl>.ApiJson("Auth0RefreshWithToken", "access_token", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0RefreshWithToken", "id_token", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0RefreshWithToken", "scope", Is.All.InstanceOf<string>());
                Step<Auth0RequestUrl>.ApiJson("Auth0RefreshWithToken", "expires_in", Is.All.InstanceOf<Int64>());
                Step<Auth0RequestUrl>.ApiJson("Auth0RefreshWithToken", "token_type", Is.All.InstanceOf<string>());
            });
        }
    }
}