﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using NUnit.Framework;
using System;
using System.Net;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Mobile EZPay Service API"), Harness("API")]
    //public class Mobile API EZPay:Prep
    public class MobileAPIEZPay
    {
        #region Preparation

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            MobileAPIEZPayUrl accountUserToken = new MobileAPIEZPayUrl();
            accountUserToken.GetAccessToken();
        }

        [SetUp]
        public new void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion Preparation

        //[Test, Description("Get all credit cards"), Author("Reginald Graham")]
        public void EZPayGetAllCardWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetAllCardWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetAllCardWithToken", Is.LessThanOrEqualTo(35));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.id", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.id", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.billingZip", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.billingZip", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.lastFour", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.lastFour", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.type", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.type", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.expiration", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetAllCardWithToken", "card.expiration", Is.All.InstanceOf<Int64>().Or.Zero);

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get all credit cards"), Author("Reginald Graham")]
        public void GetAllCreditCardsForTheUserWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetAllCardWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetAllCardWithoutToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get all credit cards"), Author("Reginald Graham")]
        public void GetAllCreditCardsForTheUserExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetAllCardExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetAllCardExpiredToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetAllCardExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        //[Test, Description("Update User Card"), Author("Reginald Graham"), Ignore("Ignore Test")]
        public void UpdateUsersCardWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayUpdateCardWithToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIEZPayUrl>.Time("EZPayUpdateCardWithToken", Is.LessThanOrEqualTo(7));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayUpdateCardWithToken", Is.EqualTo(200));
            });
        }

        //[Test, Description("Update User Card"), Author("Reginald Graham"), Ignore("Ignore Test")]
        public void UpdateUsersCardWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayUpdateCardWithoutToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIEZPayUrl>.Time("EZPayUpdateCardWithoutToken", Is.LessThanOrEqualTo(7));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayUpdateCardWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Update User Card"), Author("Reginald Graham"), Ignore("Ignore Test")]
        public void UpdateUsersCardExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayUpdateCardExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIEZPayUrl>.Time("EZPayUpdateCardExpiredToken", Is.LessThanOrEqualTo(7));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayUpdateCardExpiredToken", Is.EqualTo(401));
            });
        }

        [Test, Description("Deactivate User Card"), Author("Reginald Graham")]
        public void DeactivateUsersCardWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayDeactivateCardWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayDeactivateCardWithToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithToken", Is.EqualTo(200));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Deactivate User Card"), Author("Reginald Graham")]
        public void DeactivateUsersCardWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayDeactivateCardWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayDeactivateCardWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Deactivate User Card"), Author("Reginald Graham")]
        public void DeactivateUsersCardExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayDeactivateCardWithExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayDeactivateCardWithExpiredToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDeactivateCardWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Add Card for HPP Session Token With Access Token"), Author("Reginald Graham")]
        public void AddCardForHPPSessionTokenWithAccessToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayAddCardForHPPWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayAddCardForHPPWithToken", Is.LessThanOrEqualTo(60));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithToken", Is.EqualTo(200));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Add Card for HPP Session Token Without Access Token"), Author("Reginald Graham")]
        public void AddCardForHPPSessionTokenWithoutAccessToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayAddCardForHPPWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayAddCardForHPPWithoutToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Add Card for HPP Session Token with Expired Access Token"), Author("Reginald Graham")]
        public void AddCardForHPPSessionTokenExpiredAccessToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayAddCardForHPPExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayAddCardForHPPExpiredToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayAddCardForHPPExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Determine whether the user is allowed to setup EZ Pay"), Author("Reginald Graham")]
        public void DetermineWhetherUserIsAllowedToSetupEZPayWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.LessThanOrEqualTo(10));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "canSetupEZPay", Is.All.InstanceOf<bool>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "canSetupEZPay", Is.Not.Empty);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "canUpdateEZPay", Is.All.InstanceOf<bool>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "canUpdateEZPay", Is.Not.Empty);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "canMakeOneTimePayment", Is.All.InstanceOf<bool>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "canMakeOneTimePayment", Is.Not.Empty);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "promptToSetupEZPayWhenOnboarding", Is.All.InstanceOf<bool>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "promptToSetupEZPayWhenOnboarding", Is.Not.Empty);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "supportPhone", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "supportPhone", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "supportPhone", Is.All.Not.Empty);

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Determine whether the user is allowed to setup EZ Pay"), Author("Reginald Graham")]
        public void DetermineWhetherUserIsAllowedToSetupEZPayWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Determine whether the user is allowed to setup EZ Pay"), Author("Reginald Graham")]
        public void DetermineWhetherUserIsAllowedToSetupEZPayExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Establish EZPay Schedule"), Author("Reginald Graham")]
        public void EstablishEZPayScheduleWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.Time("EZPayEstablishEZPayScheduleWithToken", Is.LessThanOrEqualTo(12));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.FileSize("EZPayEstablishEZPayScheduleWithToken", Is.LessThanOrEqualTo(1500));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Establish EZPay Schedule"), Author("Reginald Graham")]
        public void EstablishEZPayScheduleWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayEstablishEZPayScheduleWithoutToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPayEstablishEZPayScheduleWithoutToken", Is.LessThanOrEqualTo(12));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Establish EZPay Schedule"), Author("Reginald Graham")]
        public void EstablishEZPayScheduleExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayEstablishEZPayScheduleExpiredToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPayEstablishEZPayScheduleExpiredToken", Is.LessThanOrEqualTo(12));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEstablishEZPayScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get One-Time payment options with token"), Author("Reginald Graham")]
        public void GetDetailsForSettingUpEZPayWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetDetailsForSettingUpEZPayWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetDetailsForSettingUpEZPayWithToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetDetailsForSettingUpEZPayWithToken", "scheduleText", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetDetailsForSettingUpEZPayWithToken", "amount", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get One-Time payment options without token "), Author("Reginald Graham")]
        public void GetDetailsForSettingUpEZPayWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get One-Time payment options with expired token"), Author("Reginald Graham")]
        public void GetDetailsForSettingUpEZPayExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetDetailsForSettingUpEZPayWithExpired", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham")]
        public void GetEZPayScheduleWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetScheduleWithToken", Is.LessThanOrEqualTo(750));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetScheduleWithToken", Is.LessThanOrEqualTo(30));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetScheduleWithToken", "schedules.[*].creditCardGuid", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetScheduleWithToken", "schedules.[*].startDate", Is.All.InstanceOf<string>());
                //Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetScheduleWithToken", "schedules.[*].agreementNumber", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetScheduleWithToken", "schedules.[*].amount", Is.All.InstanceOf<float>().Or.InstanceOf<Int64>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetScheduleWithToken", "schedules.[*].recurrence.id", Is.All.InstanceOf<Int64>().Or.EqualTo("None"));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayGetScheduleWithToken", "schedules.[*].recurrence.firstValue", Is.All.InstanceOf<Int64>());

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham")]
        public void GetEZPayScheduleWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetScheduleWithoutToken", Is.LessThanOrEqualTo(750));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetScheduleWithoutToken", Is.LessThanOrEqualTo(15));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham")]
        public void GetEZPayScheduleExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayGetScheduleExpiredToken", Is.LessThanOrEqualTo(750));
                Step<MobileAPIEZPayUrl>.Time("EZPayGetScheduleExpiredToken", Is.LessThanOrEqualTo(15));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayGetScheduleExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham")]
        public void EZPayPostOneTimeFuturePaymentWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayPostOneTimeFuturePaymentWithToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPayPostOneTimeFuturePaymentWithToken", Is.LessThanOrEqualTo(60));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithToken", Is.EqualTo(200));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham")]
        public void EZPayPostOneTimeFuturePaymentWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayPostOneTimeFuturePaymentWithoutToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPayPostOneTimeFuturePaymentWithoutToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham")]
        public void EZPayPostOneTimeFuturePaymentExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.EqualTo(401));
                Step<MobileAPIEZPayUrl>.FileSize("EZPayPostOneTimeFuturePaymentExpiredToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPayPostOneTimeFuturePaymentExpiredToken", Is.LessThanOrEqualTo(5));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneTimeFuturePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("EZ Pay Creates One Time Payment"), Author("Reginald Graham")]
        public void CreatesOneTimeImmediatePaymentWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.LessThanOrEqualTo(60));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.EqualTo(200));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("EZ Pay Creates One Time Payment"), Author("Reginald Graham")]
        public void CreatesOneTimeImmediatePaymentWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("EZ Pay Creates One Time Payment"), Author("Reginald Graham")]
        public void CreatesOneTimeImmediatePaymentExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        public void CreatesOneTimeImmediatePaymentWithNullCCID()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.LessThanOrEqualTo(500));
                Step<MobileAPIEZPayUrl>.Time("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.EqualTo(200));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Post One Early Payout for HPP Session Token"), Author("Reginald Graham")]
        public void PostEarlyPayoutWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.LessThanOrEqualTo(750));
                Step<MobileAPIEZPayUrl>.Time("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", "hostedPaymentUrl", Is.All.InstanceOf<string>());

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Post One Early Payout for HPP Session Token"), Author("Reginald Graham")]
        public void PostEarlyPayoutWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.LessThanOrEqualTo(750));
                Step<MobileAPIEZPayUrl>.Time("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.LessThanOrEqualTo(60));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Post One Early Payout for HPP Session Token"), Author("Reginald Graham")]
        public void PostEarlyPayoutExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.LessThanOrEqualTo(750));
                Step<MobileAPIEZPayUrl>.Time("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Gets data for the confirm one time payment screen."), Author("Reginald Graham")]
        public void SetupOneTimePaymentDetailsWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPaySetupOneTimePaymentDetailsWithToken", Is.LessThanOrEqualTo(1500).Or.Zero);
                Step<MobileAPIEZPayUrl>.Time("EZPaySetupOneTimePaymentDetailsWithToken", Is.LessThanOrEqualTo(60));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].agreementNumber", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].agreementId", Is.All.InstanceOf<Int64>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].fees.[*].amount", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].fees.[*].feeType", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].subTotal", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].tax", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "agreements.[*].totalLeaseAmount", Is.All.InstanceOf<Int64>().Or.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "creditCards.[*].id", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "creditCards.[*].billingZip", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "creditCards.[*].lastFour", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "creditCards.[*].type", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "creditCards.[*].expiration", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "memberships.[*].membershipId", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "memberships.[*].recurringAmount", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "memberships.[*].tax", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPaySetupOneTimePaymentDetailsWithToken", "totalPaymentAmount", Is.All.InstanceOf<float>().Or.Zero);

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Gets data for the confirm one time payment screen."), Author("Reginald Graham")]
        public void SetupOneTimePaymentDetailsWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsWithoutToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Gets data for the confirm one time payment screen."), Author("Reginald Graham")]
        public void SetupOneTimePaymentDetailsExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.LessThanOrEqualTo(1500));
                Step<MobileAPIEZPayUrl>.Time("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPaySetupOneTimePaymentDetailsExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Setup one time payment details"), Author("Reginald Graham")]
        public void GetEarlyPayoutDetailsWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayEarlyPayoutDetailsWithAccessToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIEZPayUrl>.Time("EZPayEarlyPayoutDetailsWithAccessToken", Is.LessThanOrEqualTo(60));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithAccessToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "totalPaymentAmount", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.agreementNumber", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.agreementId", Is.All.InstanceOf<Int64>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.sourceStore", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.productDescriptions.[*]", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.cashPrice", Is.All.InstanceOf<float>().Or.Zero);
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.costOfLeaseServices", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "agreementDetails.balancePaid", Is.All.InstanceOf<float>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "creditCards.[*].id", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "creditCards.[*].billingZip", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "creditCards.[*].lastFour", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "creditCards.[*].type", Is.All.InstanceOf<string>());
                Step<MobileAPIEZPayUrl>.ApiJson("EZPayEarlyPayoutDetailsWithAccessToken", "creditCards.[*].expiration", Is.All.InstanceOf<string>());

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithAccessToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Setup one time payment details"), Author("Reginald Graham")]
        public void GetEarlyPayoutDetailsWithNoToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayEarlyPayoutDetailsWithNoToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIEZPayUrl>.Time("EZPayEarlyPayoutDetailsWithNoToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithNoToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithNoToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Setup one time payment details"), Author("Reginald Graham")]
        public void GetEarlyPayoutDetailsExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.FileSize("EZPayEarlyPayoutDetailsWithExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIEZPayUrl>.Time("EZPayEarlyPayoutDetailsWithExpiredToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.EqualTo(401));

                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(400));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(403));
                Step<MobileAPIEZPayUrl>.StatusCode<int>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(404));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.NotFound));
                Step<MobileAPIEZPayUrl>.StatusCode<HttpStatusCode>("EZPayEarlyPayoutDetailsWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }
    }
}