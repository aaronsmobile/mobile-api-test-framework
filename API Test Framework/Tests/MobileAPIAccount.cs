﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using AutoFrameWork.Extensions;
using NUnit.Framework;
using System;
using System.Net;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Mobile API Account"), Harness("API")]
    //public class Mobile API Account:Prep
    public class MobileAPIAccount
    {
        #region Preparation

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            MobileAPIAccountUrl accountUserToken = new MobileAPIAccountUrl();
            accountUserToken.GetAccessToken();
        }

        [SetUp]
        public void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion Preparation

        [Test, Description("Test Payment History Endpoint"), Author("Reginald Graham")]
        public void AccountPaymentHistoryWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountPaymentHistoryWithToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIAccountUrl>.Time("AccountPaymentHistoryWithToken", Is.LessThanOrEqualTo(50));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountPaymentHistoryWithToken", Is.EqualTo(200));
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "header", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "header", Is.All.Not.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "header", Is.All.Contain("Your next payment of "));
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "header", Is.All.Not.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "allowPayment", Is.All.InstanceOf<bool>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "allowPayment", Is.All.Not.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "allowPayment", Is.All.Not.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].id", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].id", Is.All.Not.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].amount", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].amount", Is.All.Not.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].date", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].date", Is.All.Not.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].status", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountPaymentHistoryWithToken", "paymentList.[*].status", Is.All.Not.InstanceOf<Int64>());

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountPaymentHistoryWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountPaymentHistoryWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountPaymentHistoryWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountPaymentHistoryWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Payment History Endpoint"), Author("Reginald Graham")]
        public void AccountGetPaymentHistoryWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountPaymentHistoryWithoutToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIAccountUrl>.Time("AccountPaymentHistoryWithoutToken", Is.LessThanOrEqualTo(7));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountPaymentHistoryWithoutToken", Is.EqualTo(401));

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountPaymentHistoryWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountPaymentHistoryWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Payment History Endpoint"), Author("Reginald Graham")]
        public void AccountGetPaymentHistoryAExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountPaymentHistoryExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIAccountUrl>.Time("AccountPaymentHistoryExpiredToken", Is.LessThanOrEqualTo(15));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountPaymentHistoryExpiredToken", Is.EqualTo(401));

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountPaymentHistoryExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountPaymentHistoryExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Faqs Screen Endpoint"), Author("Reginald Graham")]
        public void AccountGetPaymentDetailWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountGetPaymentDetailWithToken", Is.LessThanOrEqualTo(3000));
                Step<MobileAPIAccountUrl>.Time("AccountGetPaymentDetailWithToken", Is.LessThanOrEqualTo(40));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetPaymentDetailWithToken", Is.EqualTo(200));
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetPaymentDetailWithToken", "receiptHTML", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetPaymentDetailWithToken", "receiptHTML", Is.Not.All.Empty);

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetPaymentDetailWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetPaymentDetailWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetPaymentDetailWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetPaymentDetailWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Faqs Screen Endpoint"), Author("Reginald Graham")]
        public void AccountGetPaymentDetailWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountGetPaymentDetailWithoutToken", Is.LessThanOrEqualTo(3000));
                Step<MobileAPIAccountUrl>.Time("AccountGetPaymentDetailWithoutToken", Is.LessThanOrEqualTo(40));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetPaymentDetailWithoutToken", Is.EqualTo(401));

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetPaymentDetailWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetPaymentDetailWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Faqs Screen Endpoint"), Author("Reginald Graham")]
        public void AccountGetPaymentDetailsExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountGetPaymentDetailsExpiredToken", Is.LessThanOrEqualTo(3000));
                Step<MobileAPIAccountUrl>.Time("AccountGetPaymentDetailsExpiredToken", Is.LessThanOrEqualTo(40));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetPaymentDetailsExpiredToken", Is.EqualTo(401));

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetPaymentDetailsExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetPaymentDetailsExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountGetProfileWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountGetProfileWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIAccountUrl>.Time("AccountGetProfileWithToken", Is.LessThanOrEqualTo(90));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithToken", Is.EqualTo(200));
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "name", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "name", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "customerSince", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "customerSince", Is.All.Contain("Valued Customer Since "));
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "customerSince", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "clubMembershipNumber", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "clubMembershipNumber", Is.All.Contain("Aaron's Club Member #").Or.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "clubMembershipNumber", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "email", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "email", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "email", Is.All.Contain("@")
                    .And.Contain(".com")
                    .Or.Contain(".net")
                    .Or.Contain(".io")
                    .Or.Contain(".ca")
                    .Or.Contain(".org")
                    .Or.Contain(".edu"));
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "cellPhone", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "cellPhone", Is.Not.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "cellPhone", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.addressLine1", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.addressLine1", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.addressLine2", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.city", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.city", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.state", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.state", Is.Not.All.Empty);
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.postalCode", Is.All.InstanceOf<string>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.postalCode", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.postalCode", Is.All.ZipCode());
                Step<MobileAPIAccountUrl>.ApiJson("AccountGetProfileWithToken", "address.postalCode", Is.Not.All.Empty);

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithToken", Is.Not.EqualTo(500));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetProfileWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetProfileWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetProfileWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountGetProfileWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountGetProfileWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIAccountUrl>.Time("AccountGetProfileWithoutToken", Is.LessThanOrEqualTo(40));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithoutToken", Is.EqualTo(401));

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetProfileWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountGetProfileWithExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountGetProfileWithExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIAccountUrl>.Time("AccountGetProfileWithExpiredToken", Is.LessThanOrEqualTo(40));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithExpiredToken", Is.EqualTo(401));

                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountGetProfileWithExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIAccountUrl>.StatusCode<HttpStatusCode>("AccountGetProfileWithExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        //[Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountUpdatePushNotifcationTrueWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountUpdatePushNotifcationTrueWithToken", Is.LessThanOrEqualTo(3000));
                Step<MobileAPIAccountUrl>.Time("AccountUpdatePushNotifcationTrueWithToken", Is.LessThanOrEqualTo(50));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountUpdatePushNotifcationTrueWithToken", Is.EqualTo(200));
            });
        }

        //[Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountUpdatePushNotificationTrueWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountUpdatePushNotificationTrueWithoutToken", Is.LessThanOrEqualTo(100));
                Step<MobileAPIAccountUrl>.Time("AccountUpdatePushNotificationTrueWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountUpdatePushNotificationTrueWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountUpdatePushNotificationTrueWithExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountUpdatePushNotificationTrueWithExpiredToken", Is.LessThanOrEqualTo(100));
                Step<MobileAPIAccountUrl>.Time("AccountUpdatePushNotificationTrueWithExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountUpdatePushNotificationTrueWithExpiredToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountUpdatePushNotifcationFalseWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountUpdatePushNotifcationFalseWithToken", Is.LessThanOrEqualTo(100));
                Step<MobileAPIAccountUrl>.Time("AccountUpdatePushNotifcationFalseWithToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountUpdatePushNotifcationFalseWithToken", Is.EqualTo(200));
            });
        }

        //[Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountUpdatePushNotificationFalseWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountUpdatePushNotificationFalseWithoutToken", Is.LessThanOrEqualTo(100));
                Step<MobileAPIAccountUrl>.Time("AccountUpdatePushNotificationFalseWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountUpdatePushNotificationFalseWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void AccountUpdatePushNotificationFalseWithExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIAccountUrl>.FileSize("AccountUpdatePushNotificationFalseWithExpiredToken", Is.LessThanOrEqualTo(100));
                Step<MobileAPIAccountUrl>.Time("AccountUpdatePushNotificationFalseWithExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIAccountUrl>.StatusCode<int>("AccountUpdatePushNotificationFalseWithExpiredToken", Is.EqualTo(401));
            });
        }
    }
}