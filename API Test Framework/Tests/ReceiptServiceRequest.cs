﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using NUnit.Framework;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Receipt Service"), Harness("API")]
    internal
    //public class Receipt Service:Prep
    class ReceiptServiceRequest
    {
        #region Preparation

        /*[OneTimeSetUp]
        public void OneTimeSetup()
        {
            //GetAuth0LoginResponse getAuth0LoginResponseObject = new GetAuth0LoginResponse();
            //getAuth0LoginResponseObject.GetAccessToken();
        }

        [SetUp]
        public new void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }*/

        #endregion Preparation

        [Test, Description("Receipt Service Small"), Author("Reginald Graham")]
        public void ReceiptServiceGetSmall()
        {
            Assert.Multiple(() =>
            {
                Step<ReceiptServiceUrl>.FileSize("ReceiptServiceGetSmallReceipt", Is.LessThanOrEqualTo(4000));
                Step<ReceiptServiceUrl>.Time("ReceiptServiceGetSmallReceipt", Is.LessThanOrEqualTo(30));
                Step<ReceiptServiceUrl>.StatusCode<int>("ReceiptServiceGetSmallReceipt", Is.EqualTo(200));
                Step<ReceiptServiceUrl>.ApiJson("ReceiptServiceGetSmallReceipt", "htmlString", Is.All.EqualTo("<style>\r\n                .monospace {\r\n                font-family: Courier, monospace;\r\n                font-size: 24px;\r\n                font-style: normal;\r\n                font-variant: normal;\r\n                font-weight: 500;\r\n                line-height: 26.4px;\r\n            }\r\n            </style><div class=\"monospace\">****************************************<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aaron's<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C0974<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4543&nbsp;Street&nbsp;Name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suite&nbsp;#2<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Atlanta,&nbsp;GA&nbsp;30341<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;111-111-1111<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment<br>Receipt&nbsp;#:&nbsp;45435<br>&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;:&nbsp;8/21/2017<br>&nbsp;&nbsp;&nbsp;&nbsp;Time&nbsp;:&nbsp;12:00&nbsp;AM<br><br>Test&nbsp;First&nbsp;Name&nbsp;Test&nbsp;Last&nbsp;Name<br>Address&nbsp;Line&nbsp;1<br>Address&nbsp;Line&nbsp;2<br>Atlanta,&nbsp;GA&nbsp;30341<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THANK&nbsp;YOU&nbsp;FOR&nbsp;YOUR&nbsp;PAYMENT!<br><br><br><br>&nbsp;&nbsp;Club&nbsp;Membership#&nbsp;&nbsp;&nbsp;:6546456<br>&nbsp;&nbsp;Amt&nbsp;Paid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.50<br>&nbsp;&nbsp;HST&nbsp;Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.50<br>&nbsp;&nbsp;Next&nbsp;Due&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:8/21/2017<br><br>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;10.00<br><br>Cash:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.00<br><br>VISA:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;10.00<br><br>Store&nbsp;Credit:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;15.00<br><br><br>Total&nbsp;Amount&nbsp;Tendered&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;30.00<br>Total&nbsp;Amount&nbsp;Due&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;30.00<br>Total&nbsp;Change&nbsp;Due&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00<br><br><br><br>FOOTER<br>****************************************<br><br><br><br><br><br><br><br><br></div>").And.Not.Empty);
            });
        }

        [Test, Category("Receipt Service"), Description("Receipt Service Large"), Author("Reginald Graham")]
        public void ReceiptServiceGetLarge()
        {
            Assert.Multiple(() =>
            {
                Step<ReceiptServiceUrl>.FileSize("ReceiptServiceGetLargeReceipt", Is.LessThanOrEqualTo(4000));
                Step<ReceiptServiceUrl>.Time("ReceiptServiceGetLargeReceipt", Is.LessThanOrEqualTo(40));
                Step<ReceiptServiceUrl>.StatusCode<int>("ReceiptServiceGetLargeReceipt", Is.EqualTo(200));
                Step<ReceiptServiceUrl>.ApiJson("ReceiptServiceGetLargeReceipt", "htmlString", Is.All.EqualTo("<style>\r\n                .monospace {\r\n                font-family: Courier, monospace;\r\n                font-size: 24px;\r\n                font-style: normal;\r\n                font-variant: normal;\r\n                font-weight: 500;\r\n                line-height: 26.4px;\r\n            }\r\n            </style><div class=\"monospace\">****************************************<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Aaron's<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;C0974<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4543&nbsp;Street&nbsp;Name<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suite&nbsp;#2<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Atlanta,&nbsp;GA&nbsp;30341<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;111-111-1111<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment<br>Receipt&nbsp;#:&nbsp;45435<br>&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;:&nbsp;8/21/2017<br>&nbsp;&nbsp;&nbsp;&nbsp;Time&nbsp;:&nbsp;12:00&nbsp;AM<br><br>Test&nbsp;First&nbsp;Name&nbsp;Test&nbsp;Last&nbsp;Name<br>Address&nbsp;Line&nbsp;1<br>Address&nbsp;Line&nbsp;2<br>Atlanta,&nbsp;GA&nbsp;30341<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THANK&nbsp;YOU&nbsp;FOR&nbsp;YOUR&nbsp;PAYMENT!<br><br><br>&nbsp;&nbsp;Agreement#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:23544<br>&nbsp;&nbsp;Amt&nbsp;Paid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.50<br>&nbsp;&nbsp;Non-Renewal&nbsp;Fee&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.00<br>&nbsp;&nbsp;Deferred&nbsp;NR&nbsp;Fee&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.00<br>&nbsp;&nbsp;In&nbsp;Home&nbsp;Visit&nbsp;Fee&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.50<br>&nbsp;&nbsp;Delivery&nbsp;Fee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.00<br>&nbsp;&nbsp;Disposal&nbsp;Fee&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;15.00<br>&nbsp;&nbsp;HST&nbsp;Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.30<br>&nbsp;&nbsp;Next&nbsp;Due&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:10/21/2017<br>&nbsp;&nbsp;Early&nbsp;Payout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;500.00<br>&nbsp;&nbsp;EPO&nbsp;Good&nbsp;Through&nbsp;&nbsp;&nbsp;:10/21/2017<br>&nbsp;&nbsp;Payments&nbsp;Remaining&nbsp;:5.81<br>&nbsp;&nbsp;Frequency&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:Monthly<br>Same&nbsp;As&nbsp;Cash&nbsp;Expires&nbsp;:8/21/2027<br><br><br>&nbsp;&nbsp;Club&nbsp;Membership#&nbsp;&nbsp;&nbsp;:58454<br>&nbsp;&nbsp;Total&nbsp;Paid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.00<br><br>&nbsp;&nbsp;Club&nbsp;Membership#&nbsp;&nbsp;&nbsp;:6546456<br>&nbsp;&nbsp;Amt&nbsp;Paid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.50<br>&nbsp;&nbsp;HST&nbsp;Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.50<br>&nbsp;&nbsp;Next&nbsp;Due&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:8/21/2017<br><br>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;52.30<br><br>Cash:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.00<br><br>VISA:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;10.00<br><br>Store&nbsp;Credit:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;15.00<br><br><br>Total&nbsp;Amount&nbsp;Tendered&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;30.00<br>Total&nbsp;Amount&nbsp;Due&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;30.00<br>Total&nbsp;Change&nbsp;Due&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;0.00<br><br><br><br>FOOTER<br>****************************************<br><br><br><br><br><br><br><br><br></div>").And.Not.Empty);
            });
        }

        //[Test, Category("Receipt Service"), Description("Receipt Service Specific"), Author("Reginald Graham")]
        public void ReceiptServiceGetSpecific()
        {
            Assert.Multiple(() =>
            {
                Step<ReceiptServiceUrl>.FileSize("ReceiptServiceGetSpecificReceipt", Is.LessThanOrEqualTo(4000));
                Step<ReceiptServiceUrl>.Time("ReceiptServiceGetSpecificReceipt", Is.LessThanOrEqualTo(40));
                Step<ReceiptServiceUrl>.StatusCode<int>("ReceiptServiceGetSpecificReceipt", Is.EqualTo(200));
                Step<ReceiptServiceUrl>.ApiJson("ReceiptServiceGetSpecificReceipt", "htmlString", Is.All.EqualTo("<style>\r\n                .monospace {\r\n                font-family: Courier, monospace;\r\n                font-size: 24px;\r\n                font-style: normal;\r\n                font-variant: normal;\r\n                font-weight: 500;\r\n                line-height: 26.4px;\r\n            }\r\n            </style><div class=\"monospace\">****************************************<br>&nbsp;&nbsp;&nbsp;&nbsp;Aaron's&nbsp;Sales&nbsp;and&nbsp;Lease&nbsp;Ownership<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;F284<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2523&nbsp;E&nbsp;MAIN&nbsp;ST<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FARMINGTON,&nbsp;NM&nbsp;87401<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;505-324-0002<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Payment<br>Receipt&nbsp;#:&nbsp;500569<br>&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;:&nbsp;11/2/2016<br>&nbsp;&nbsp;&nbsp;&nbsp;Time&nbsp;:&nbsp;3:33&nbsp;PM<br><br>COLLETTE&nbsp;CHARLIE<br>17&nbsp;Road&nbsp;3319<br>Aztec,&nbsp;NM&nbsp;87410<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;THANK&nbsp;YOU&nbsp;FOR&nbsp;YOUR&nbsp;PAYMENT!<br><br><br>&nbsp;&nbsp;Agreement#&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:65505<br>&nbsp;&nbsp;Amt&nbsp;Paid&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;80.25<br>&nbsp;&nbsp;Tax&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;7.08<br>&nbsp;&nbsp;Early&nbsp;Payout&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;Paid&nbsp;in&nbsp;Full<br><br><br>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;&nbsp;87.33<br><br>VISA:<br>&nbsp;&nbsp;Amount&nbsp;Tendered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:$&nbsp;&nbsp;&nbsp;100.00<br><br><br>Total&nbsp;Amount&nbsp;Tendered&nbsp;:$&nbsp;&nbsp;&nbsp;100.00<br><br><br><br>Your&nbsp;feedback&nbsp;is&nbsp;important&nbsp;to&nbsp;Aaron's<br>Contact&nbsp;us&nbsp;at:<br>myexperience@aarons.com<br>****************************************<br><br><br><br><br><br><br><br><br></div>").And.Not.Empty);
            });
        }
    }
}