﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using AutoFrameWork.Extensions;
using NUnit.Framework;
using System;
using System.Net;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Mobile API OnBoarding"), Harness("API")]
    //public class Mobile API OnBoarding:Prep
    public class MobileAPIOnBoarding
    {
        #region Preparation

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            MobileAPIOnBoardingUrl accountUserToken = new MobileAPIOnBoardingUrl();
            accountUserToken.GetAccessToken();
        }

        [SetUp]
        public new void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion Preparation

        [Test, Description("Test Add User OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingCanCustomerLoginWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingCanUserLoginWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingCanUserLoginWithToken", Is.LessThanOrEqualTo(15));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingCanUserLoginWithToken", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingCanUserLoginWithToken", "canUserLogin", Is.All.InstanceOf<bool>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingCanUserLoginWithToken", "hoursSinceUserCreated", Is.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingCanUserLoginWithToken", "hoursBeforeNewUserValidity", Is.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingCanUserLoginWithToken", "isUserWaitingPeriodOver", Is.All.InstanceOf<bool>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingCanUserLoginWithToken", "isUserEmailVerified", Is.All.InstanceOf<bool>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingCanUserLoginWithToken", "doesUserExist", Is.All.InstanceOf<bool>());

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingCanUserLoginWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingCanUserLoginWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingCanUserLoginWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingCanUserLoginWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Add User OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingCanCustomerLoginWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingCanUserLoginWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingCanUserLoginWithoutToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingCanUserLoginWithoutToken", Is.EqualTo(401));

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingCanUserLoginWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingCanUserLoginWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Add User OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingCanCustomerLoginExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingCanUserLoginExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingCanUserLoginExpiredToken", Is.LessThanOrEqualTo(30));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingCanUserLoginExpiredToken", Is.EqualTo(401));

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingCanUserLoginExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingCanUserLoginExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test GetAgreements OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingFindCustomerWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingFindCustomerWithToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingFindCustomerWithToken", Is.LessThanOrEqualTo(40));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingFindCustomerWithToken", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].agreementId", Is.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].agreementId", Is.Not.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].agreementId", Is.Not.Empty);
                //Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].title", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].agreementNumber", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].agreementNumber", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].agreementNumber", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].itemCount", Is.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].itemCount", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.addressLine1", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.addressLine1", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.city", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.city", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.state", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.state", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.postalCode", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.postalCode", Is.All.ZipCode());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.postalCode", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].storeDetails.address.postalCode", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "agreements.[*].startDate", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "globalCustomerId", Is.All.InstanceOf<string>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "globalCustomerId", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingFindCustomerWithToken", "globalCustomerId", Is.Not.Empty);

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingFindCustomerWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingFindCustomerWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingFindCustomerWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingFindCustomerWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test GetAgreements OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingFindCustomerWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingFindCustomerWithoutToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingFindCustomerWithoutToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingFindCustomerWithoutToken", Is.EqualTo(401));

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingFindCustomerWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingFindCustomerWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test GetAgreements OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingFindCustomerExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingFindCustomerExpiredToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingFindCustomerExpiredToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingFindCustomerExpiredToken", Is.EqualTo(401));

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingFindCustomerExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingFindCustomerExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Find Customer OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingSetCustomerIdWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingSetCustomerIdWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingSetCustomerIdWithToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingSetCustomerIdWithToken", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingSetCustomerIdWithToken", "userIsUpdated", Is.All.InstanceOf<bool>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingSetCustomerIdWithToken", "userIsUpdated", Is.Not.Empty);
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingSetCustomerIdWithToken", "doesUserExist", Is.All.InstanceOf<bool>());
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingSetCustomerIdWithToken", "doesUserExist", Is.Not.Empty);

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingSetCustomerIdWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingSetCustomerIdWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingSetCustomerIdWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingSetCustomerIdWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Find Customer OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingSetCustomerIdWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingSetCustomerIdWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingSetCustomerIdWithoutToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingSetCustomerIdWithoutToken", Is.EqualTo(401));

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingSetCustomerIdWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingSetCustomerIdWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Find Customer OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingSetCustomerIdExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingSetCustomerIdExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingSetCustomerIdExpiredToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingSetCustomerIdExpiredToken", Is.EqualTo(401));

                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingSetCustomerIdExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.StatusCode<HttpStatusCode>("OnBoardingSetCustomerIdExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        public void OnBoardingCanCustomerLoginUserInvalidTrue()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingcanUserLoginuseInvalidTrue", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingcanUserLoginuseInvalidTrue", Is.LessThanOrEqualTo(15));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingcanUserLoginuseInvalidTrue", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidTrue", "canUserLogin", Does.Contain(false));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidTrue", "hoursSinceUserCreated", Does.Contain(0));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidTrue", "hoursBeforeNewUserValidity", Does.Contain(24));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidTrue", "isUserWaitingPeriodOver", Does.Contain(false));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidTrue", "isUserEmailVerified", Does.Contain(false));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidTrue", "doesUserExist", Does.Contain(true));
            });
        }

        public void OnBoardingCanCustomerLoginUserInvalidFalse()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingcanUserLoginuseInvalidFalse", Is.LessThanOrEqualTo(500));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingcanUserLoginuseInvalidFalse", Is.LessThanOrEqualTo(25));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingcanUserLoginuseInvalidFalse", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidFalse", "canUserLogin", Does.Contain(true));
                //Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidFalse", "hoursSinceUserCreated", Does.Contain(24));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidFalse", "hoursBeforeNewUserValidity", Does.Contain(24));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidFalse", "isUserWaitingPeriodOver", Does.Contain(true));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidFalse", "isUserEmailVerified", Does.Contain(true));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingcanUserLoginuseInvalidFalse", "doesUserExist", Does.Contain(false));
            });
        }

        public void OnBoardingCreatedYesterdayWithin24HourPeriod()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", Is.LessThanOrEqualTo(200));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", Is.LessThanOrEqualTo(2));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "canUserLogin", Does.Contain(false));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "hoursSinceUserCreated", Does.Contain(23));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "hoursBeforeNewUserValidity", Is.All.GreaterThan(24));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "isUserWaitingPeriodOver", Does.Contain(false));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "isUserEmailVerified", Does.Contain(true));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "doesUserExist", Does.Contain(true));
            });
        }

        [Test, Description("Test OnBoarding Functionality"), Author("Reginald Graham")]
        public void OnBoardingCreatedYesterdayOutside24HourPeriod()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIOnBoardingUrl>.FileSize("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", Is.LessThanOrEqualTo(200));
                Step<MobileAPIOnBoardingUrl>.Time("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", Is.LessThanOrEqualTo(25));
                Step<MobileAPIOnBoardingUrl>.StatusCode<int>("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", Is.EqualTo(200));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "canUserLogin", Does.Contain(true));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "hoursSinceUserCreated", Is.All.GreaterThanOrEqualTo(24));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "hoursBeforeNewUserValidity", Does.Contain(24));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "isUserWaitingPeriodOver", Does.Contain(true));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "isUserEmailVerified", Does.Contain(true));
                Step<MobileAPIOnBoardingUrl>.ApiJson("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "doesUserExist", Does.Contain(true));
            });
        }
    }
}