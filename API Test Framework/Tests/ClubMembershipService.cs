﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using NUnit.Framework;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Club Membership Service"), Harness("API")]
    //public class Mobile API Club Membership:Prep
    public class ClubMembershipService
    {
        #region Preparation

        /*[OneTimeSetUp]
        public void OneTimeSetup()
        {
            base.OneTimeSetup(new StackTrace().GetFrame(1).GetMethod());
        }

        [SetUp]
        public new void Setup()
        {
            base.Setup();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            base.OneTimeTearDown("Ecomm API Tests: ");
        }

        [TearDown]
        public void TearDown()
        {
            var type = Type.GetType(TestContext.CurrentContext.Test.ClassName);
            TearDown(type);
        }*/

        #endregion Preparation

        [Test, Description("Club Membership Search Memberships"), Author("Reginald Graham")]
        public void ClubMembershipServiceSearchMembershipsActive()
        {
            Assert.Multiple(() =>
            {
                Step<ClubMemberServiceUrl>.FileSize("ClubMembershipServiceSearchMembershipsActive", Is.LessThanOrEqualTo(35000));
                Step<ClubMemberServiceUrl>.Time("ClubMembershipServiceSearchMembershipsActive", Is.LessThanOrEqualTo(1));
                Step<ClubMemberServiceUrl>.StatusCode<int>("ClubMembershipServiceSearchMembershipsActive", Is.EqualTo(200));
            });
        }

        [Test, Description("Club Membership Get Memberships Active"), Author("Reginald Graham")]
        public void ClubMembershipServiceGetMembershipsActive()
        {
            Assert.Multiple(() =>
            {
                Step<ClubMemberServiceUrl>.FileSize("ClubMembershipServiceGetMembershipsActive", Is.LessThanOrEqualTo(3500));
                Step<ClubMemberServiceUrl>.Time("ClubMembershipServiceGetMembershipsActive", Is.LessThanOrEqualTo(1));
                Step<ClubMemberServiceUrl>.StatusCode<int>("ClubMembershipServiceGetMembershipsActive", Is.EqualTo(200));
            });
        }

        [Test, Description("Club Membership Recurrence Patterns"), Author("Reginald Graham")]
        public void ClubMembershipServiceRecurrencePatterns()
        {
            Assert.Multiple(() =>
            {
                Step<ClubMemberServiceUrl>.FileSize("ClubMembershipServiceReccurencePatterns", Is.LessThanOrEqualTo(1500));
                Step<ClubMemberServiceUrl>.Time("ClubMembershipServiceReccurencePatterns", Is.LessThanOrEqualTo(1));
                Step<ClubMemberServiceUrl>.StatusCode<int>("ClubMembershipServiceReccurencePatterns", Is.EqualTo(200));
            });
        }
    }
}