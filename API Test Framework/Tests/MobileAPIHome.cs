﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using AutoFrameWork.Extensions;
using NUnit.Framework;
using System;
using System.Net;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Mobile API Home"), Harness("API")]
    //public class Mobile API Home:Prep
    public class MobileAPIHome
    {
        #region Preparation

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            MobileAPIHomeUrl accountUserToken = new MobileAPIHomeUrl();
            accountUserToken.GetAccessToken();
        }

        [SetUp]
        public new void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion Preparation

        [Test, Description("Get Customer Agreement Detail Request"), Author("Reginald Graham")]
        public void HomeGetCustomerAgreementDetailRequestWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerAgreementDetailsWithToken", Is.LessThanOrEqualTo(200));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerAgreementDetailsWithToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsWithToken", Is.EqualTo(200));
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementNumber", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementNumber", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementNumber", Is.Not.All.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementId", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementId", Is.Not.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementId", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sourceStore", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sourceStore", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementItems.[*].description", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementItems.[*].sku", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "agreementItems.[*].sku", Is.Not.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sameAsCash.duration", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sameAsCash.duration", Is.Not.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sameAsCash.durationType", Does.Contain("Daily").Or.Contain("Monthly"));
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sameAsCash.endDate", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "sameAsCash.daysCompletedPercentage", Is.All.InstanceOf<Int64>().Or.All.InstanceOf<float>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "nextPaymentAmount", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "nextPaymentAmount", Is.Not.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "nextPaymentAmount", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "nextRenewalDate", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "nextRenewalDate", Is.Not.All.InstanceOf<DateTime>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "recurrence", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "completedPaymentsCount", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "completedPaymentsCount", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "leaseTerm", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "leaseTerm", Is.Not.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "leaseTerm", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "startDate", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "startDate", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "endDate", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "endDate", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "daysCompletedPercentage", Is.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "daysCompletedPercentage", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.sourceStore", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.sourceStore", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.addressLine1", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.addressLine1", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.city", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.city", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.state", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.state", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.postalCode", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.postalCode", Is.All.ZipCode());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.postalCode", Is.Not.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.address.postalCode", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.phone", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.phone", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "storeDetails.phone", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "normalPay", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "normalPay", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "cashPrice", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "cashPrice", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "costOfLeaseServices", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "costOfLeaseServices", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "totalCostToFullTerm", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "totalCostToFullTerm", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "balancePaid", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "balancePaid", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "balanceRemaining", Is.All.InstanceOf<float>().Or.Zero.Or.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "balanceRemaining", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerAgreementDetailsWithToken", "hasEZPay", Is.All.InstanceOf<bool>());

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerAgreementDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerAgreementDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerAgreementDetailsWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Customer Agreement Detail Request"), Author("Reginald Graham")]
        public void HomeGetCustomerAgreementDetailRequestWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerAgreementDetailsWithoutToken", Is.LessThanOrEqualTo(200));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerAgreementDetailsWithoutToken", Is.LessThanOrEqualTo(2));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsWithoutToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerAgreementDetailsWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Get Customer Agreement Detail Request"), Author("Reginald Graham")]
        public void HomeGetCustomerAgreementDetailRequestExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerAgreementDetailsExpiredToken", Is.LessThanOrEqualTo(200));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerAgreementDetailsExpiredToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsExpiredToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerAgreementDetailsExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Get Customer Agreement Detail Request"), Author("Reginald Graham")]
        public void HomeGetCustomerAgreementDetailsInactiveAgreement()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerAgreementDetailsInactiveAgreementToken", Is.LessThanOrEqualTo(200));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerAgreementDetailsInactiveAgreementToken", Is.LessThanOrEqualTo(45));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsInactiveAgreementToken", Is.EqualTo(400));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerAgreementDetailsInactiveAgreementToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerAgreementDetailsInactiveAgreementToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Get Customer Summary Request"), Author("Reginald Graham")]
        public void HomeGetCustomerSummaryRequestWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerSummaryRequestWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerSummaryRequestWithToken", Is.LessThanOrEqualTo(10));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerSummaryRequestWithToken", Is.EqualTo(200));
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].agreementId", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].agreementId", Is.Not.All.InstanceOf<string>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].agreementId", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sourceStore", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sourceStore", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].title", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].title", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].itemCount", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].itemCount", Is.Not.All.InstanceOf<float>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].leaseTerm", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].leaseTerm", Is.Not.All.InstanceOf<float>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].completedPaymentsCount", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].completedPaymentsCount", Is.Not.All.InstanceOf<float>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].daysCompletedPercentage", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].daysCompletedPercentage", Is.Not.All.InstanceOf<float>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sameAsCash.duration", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sameAsCash.durationType", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sameAsCash.durationType", Does.Contain("Monthly").Or.Contain("Daily"));
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sameAsCash.endDate", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "agreementList.[*].sameAsCash.daysCompletedPercentage", Is.All.InstanceOf<Int64>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "nextPaymentAmount", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "nextPaymentAmount", Is.Not.All.InstanceOf<Int64>().Or.Zero);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "nextPaymentAmount", Is.Not.Empty);
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "firstName", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetCustomerSummaryRequestWithToken", "firstName", Is.Not.Empty);

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerSummaryRequestWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerSummaryRequestWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerSummaryRequestWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerSummaryRequestWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Get Customer Summary Request"), Author("Reginald Graham")]
        public void HomeGetCustomerSummaryRequestWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerSummaryRequestWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerSummaryRequestWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerSummaryRequestWithoutToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerSummaryRequestWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerSummaryRequestWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Get Customer Summary Request"), Author("Reginald Graham")]
        public void HomeGetCustomerSummaryRequestExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetCustomerSummaryRequestExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeGetCustomerSummaryRequestExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerSummaryRequestExpiredToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetCustomerSummaryRequestExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetCustomerSummaryRequestExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        //[Test, Description("Get Partial Pay Options Request"), Author("Reginald Graham")]
        public void HomeGetPartialPayOptionsRequestWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetPartialPayOptionsRequestWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeGetPartialPayOptionsRequestWithToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPartialPayOptionsRequestWithToken", Is.EqualTo(200));
            });
        }

        //[Test, Description("Get Partial Pay Options Request"), Author("Reginald Graham")]
        public void HomeGetPartialPayOptionsRequestWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetPartialPayOptionsRequestWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeGetPartialPayOptionsRequestWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPartialPayOptionsRequestWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Get Partial Pay Options Request"), Author("Reginald Graham")]
        public void HomeGetPartialPayOptionsRequestExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetPartialPayOptionsRequestExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeGetPartialPayOptionsRequestExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPartialPayOptionsRequestExpiredToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Get Promise To Pay Request"), Author("Reginald Graham")]
        public void HomeAddPartialPaymentRequestWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeAddPartialPaymentRequestWithToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeAddPartialPaymentRequestWithToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPartialPaymentRequestWithToken", Is.EqualTo(200));
            });
        }

        //[Test, Description("Get Promise To Pay Request"), Author("Reginald Graham")]
        public void HomeAddPartialPaymentRequestWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeAddPartialPaymentRequestWithoutToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeAddPartialPaymentRequestWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPartialPaymentRequestWithoutToken", Is.EqualTo(401));
            });
        }

        //[Test, Description("Get Promise To Pay Request"), Author("Reginald Graham")]
        public void HomeAddPartialPaymentRequestExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeAddPartialPaymentRequestExpiredToken", Is.LessThanOrEqualTo(500));
                Step<MobileAPIHomeUrl>.Time("HomeAddPartialPaymentRequestExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPartialPaymentRequestExpiredToken", Is.EqualTo(401));
            });
        }

        [Test, Description("Test Promise to Pay endpoint"), Author("Reginald Graham")]
        public void HomeAddPromiseToPayWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeAddPromiseToPayWithToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIHomeUrl>.Time("HomeAddPromiseToPayWithToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPromiseToPayWithToken", Is.EqualTo(200));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPromiseToPayWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeAddPromiseToPayWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeAddPromiseToPayWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeAddPromiseToPayWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Promise to Pay endpoint"), Author("Reginald Graham")]
        public void HomeAddPromiseToPayWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeAddPromiseToPayWithoutToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIHomeUrl>.Time("HomeAddPromiseToPayWithoutToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPromiseToPayWithoutToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPromiseToPayWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeAddPromiseToPayWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Promise to Pay endpoint"), Author("Reginald Graham")]
        public void HomeAddPromiseToPayExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeAddPromiseToPayExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIHomeUrl>.Time("HomeAddPromiseToPayExpiredToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPromiseToPayExpiredToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeAddPromiseToPayExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeAddPromiseToPayExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Promise to Pay Options endpoint"), Author("Reginald Graham")]
        public void HomeGetPromiseToPayOptionsRequestWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetPromiseToPayOptionsRequestWithToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIHomeUrl>.Time("HomeGetPromiseToPayOptionsRequestWithToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPromiseToPayOptionsRequestWithToken", Is.EqualTo(200));
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetPromiseToPayOptionsRequestWithToken", "latestValidDate", Is.All.InstanceOf<string>());
                Step<MobileAPIHomeUrl>.ApiJson("HomeGetPromiseToPayOptionsRequestWithToken", "payAmount", Is.All.InstanceOf<float>().Or.Zero);

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPromiseToPayOptionsRequestWithToken", Is.Not.EqualTo(400));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetPromiseToPayOptionsRequestWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetPromiseToPayOptionsRequestWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetPromiseToPayOptionsRequestWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetPromiseToPayOptionsRequestWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Promise to Pay Options endpoint"), Author("Reginald Graham")]
        public void HomeGetPromiseToPayOptionsRequestWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetPromiseToPayOptionsRequestWithoutToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIHomeUrl>.Time("HomeGetPromiseToPayOptionsRequestWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPromiseToPayOptionsRequestWithoutToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPromiseToPayOptionsRequestWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetPromiseToPayOptionsRequestWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Promise to Pay Options endpoint"), Author("Reginald Graham")]
        public void HomeGetPromiseToPayOptionsRequestExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIHomeUrl>.FileSize("HomeGetPromiseToPayOptionsRequestExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPIHomeUrl>.Time("HomeGetPromiseToPayOptionsRequestExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPromiseToPayOptionsRequestExpiredToken", Is.EqualTo(401));

                Step<MobileAPIHomeUrl>.StatusCode<int>("HomeGetPromiseToPayOptionsRequestExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPIHomeUrl>.StatusCode<HttpStatusCode>("HomeGetPromiseToPayOptionsRequestExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }
    }
}