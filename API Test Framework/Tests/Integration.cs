﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using AutoFrameWork.Extensions;
using NUnit.Framework;
using System;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Integration Tests"), Harness("API")]
    public class Integration
    {
        private IntegrationUrl integrationObject = new IntegrationUrl();

        #region Preparation

        /*[OneTimeSetUp]
        public void OneTimeSetup()
        {
            base.OneTimeSetup(new StackTrace().GetFrame(1).GetMethod());
        }

        [SetUp]
        public new void Setup()
        {
            base.Setup();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            base.OneTimeTearDown("Ecomm API Tests: ");
        }

        [TearDown]
        public void TearDown()
        {
            var type = Type.GetType(TestContext.CurrentContext.Test.ClassName);
            TearDown(type);
        }*/

        #endregion Preparation

        //[Test, Description("Get all credit cards"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void MobileAPIEZPayGetAllCreditCardsForTheUserSpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("MobileAPIEZPayGetAllCardsWithForSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetAllCardsWithForSpecificUser", "cards.[*].id", Does.Contain("1234567"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetAllCardsWithForSpecificUser", "cards.[*].billingZip", Does.Contain("30301"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetAllCardsWithForSpecificUser", "cards.[*].lastFour", Does.Contain("1234"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetAllCardsWithForSpecificUser", "cards.[*].type", Does.Contain("Visa"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetAllCardsWithForSpecificUser", "cards.[*].masked", Does.Contain("####2982####0801"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetAllCardsWithForSpecificUser", "cards.[*].expiration", Does.Contain("09/20"));
            });
        }

        //[Test, Description("User can setup EZ Pay"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void MobileAPIEZPayDetermineWhetherASpecificUserCanSetupEZPay()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("MobileAPIEZAPIDetermineIfUserAllowedToSetupEZPayForSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZAPIDetermineIfUserAllowedToSetupEZPayForSpecificUser", "canSetupEZPay", Does.Contain(false));
            });
        }

        //[Test, Description("Test GetAgreements OnBoarding Functionality"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void MobileAPIOnBoardingGetAgreementsForASpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("MobileAPIOnBoardingGetAgreementsSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("MobileAPIOnBoardingGetAgreementsSpecificUser", "agreements.[*].name", Does.Contain("Agreement 1"));
                Step<IntegrationUrl>.ApiJson("MobileAPIOnBoardingGetAgreementsSpecificUser", "agreements.[*].id", Does.Contain(223344));
                Step<IntegrationUrl>.ApiJson("MobileAPIOnBoardingGetAgreementsSpecificUser", "agreements.[*].itemCount", Does.Contain(1));
                Step<IntegrationUrl>.ApiJson("MobileAPIOnBoardingGetAgreementsSpecificUser", "agreements.[*].storeLocation", Does.Contain("Store222"));
                Step<IntegrationUrl>.ApiJson("MobileAPIOnBoardingGetAgreementsSpecificUser", "agreements.[*].startDate", Does.Contain("/Date(1525865927321-0400)/"));
            });
        }

        //[Test, Description("Get Data For Confirm Later Payment Setup"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void MobileAPIEZPayGetEZPayScheduleWithForASpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("MobileAPIEZPayGetScheduleSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].creditCardGuid", Does.Contain("1b26b8c99ba0481fb8f1c3c25c5f0b13"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].startDate", Does.Contain("/ Date(1525865927321 - 0400) /"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].agreementNumber", Does.Contain("223344"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].store", Does.Contain("Store222"));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].amount", Does.Contain(10.0));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].recurrence.id", Does.Contain(1));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayGetScheduleSpecificUser", "schedules.[*].recurrence.firstValue", Does.Contain(1));
            });
        }

        //[Test, Description("Gets Data For Confirm One Time Payment"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void PostOneTimePaymentDetailsforHPPSessionTokenSpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("MobileAPIEZPayPostOneTimePaymentDetailsForHPPSessionSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayPostOneTimePaymentDetailsForHPPSessionSpecificUser", "payment.creditCardType", Does.Contain(00000000000000000000000000000000));
            });
        }

        //[Test, Description("Gets Data For Confirm One Time Payment"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void CreatesAOneTimePaymentSpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("MobileAPIEZPayPostOneTimePaymentDetailsForHPPSessionSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("MobileAPIEZPayPostOneTimePaymentDetailsForHPPSessionSpecificUser", "responseStatus", Is.Empty);
            });
        }

        //[Test, Description("Auth0 Update User"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void Auth0UpdateUserSpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<IntegrationUrl>.StatusCode<int>("Auth0UpdateUserSpecificUser", Is.EqualTo(200));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "email_verified", Does.Contain(true));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "email", Does.Contain("aaronstestuser01@gmail.com"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "updated_at", Is.All.TimeFormat24Hour());
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "name", Does.Contain("aaronstestuser01@gmail.com"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "picture", Does.Contain("https://s.gravatar.com/avatar/d8f2da49703e3b58f38a36bf9af9672d?s=480&r=pg&d=https%3A%2F%2Fcdn.auth0.com%2Favatars%2Faa.png"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "user_id", Does.Contain("auth0|5af2f1d8e1fee0667008fe30"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "nickname", Does.Contain("aaronstestuser01"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "identities.[*].user_id", Does.Contain("5af2f1d8e1fee0667008fe30"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "identities.[*].provider", Does.Contain("auth0"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "identities.[*].connection", Does.Contain("Username-Password-Authentication"));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "identities.[*].isSocial", Does.Contain(false));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "created_at", Is.All.TimeFormat24Hour());
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "user_metadata.GlobalCustomerId", Does.Contain(200));
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "last_ip", Is.All.InstanceOf<string>());
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "last_login", Is.All.TimeFormat24Hour());
                Step<IntegrationUrl>.ApiJson("Auth0UpdateUserSpecificUser", "logins_count", Is.All.InstanceOf<Int64>());
            });
        }

        //[Test, Description("Update User Card"), Author("Reginald Graham"), Ignore("Ignore test")]
        public void UpdateUsersCardSpecificUser()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPIEZPayUrl>.StatusCode<int>("MobileAPIEZPayUpdateCardWithToken", Is.EqualTo(200));
                Step<MobileAPIEZPayUrl>.StatusCode<string>("MobileAPIEZPayUpdateCardWithToken", Is.EqualTo("OK"));
            });
        }
    }
}