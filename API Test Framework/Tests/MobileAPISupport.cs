﻿using API_Test_Framework.Apis;
using AutoFrameWork.Attributes;
using AutoFrameWork.Core;
using NUnit.Framework;
using System;
using System.Net;
using static API_Test_Framework.Settings.Attributes;

namespace API_Test_Framework.Tests
{
    [Tribe("Digital Leasing Solutions/Mobile"), Category("Mobile API Support"), Harness("API")]
    //public class Mobile API Support:Prep
    public class MobileAPISupport
    {
        #region Preparation

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            MobileAPISupportUrl accountUserToken = new MobileAPISupportUrl();
            accountUserToken.GetAccessToken();
        }

        [SetUp]
        public new void Setup()
        {
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
        }

        [TearDown]
        public void TearDown()
        {
        }

        #endregion Preparation

        [Test, Description("Test Tips Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetTipsWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportGetTipsWithToken", Is.LessThanOrEqualTo(20));
                Step<MobileAPISupportUrl>.Time("SupportGetTipsWithToken", Is.LessThanOrEqualTo(3));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetTipsWithToken", Is.EqualTo(200));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "header", Does.Contain("Top Things To Know."));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "header", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "subHeader", Does.Contain("A few advantages of your Aaron's lease:"));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "subHeader", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "tips.[*].sortOrder", Is.All.InstanceOf<Int64>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "tips.[*].sortOrder", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "tips.[*].title", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "tips.[*].title", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "tips.[*].description", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "tips.[*].description", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTipsHeader", Does.Contain("And More..."));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTipsHeader", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTips.[*].sortOrder", Is.All.InstanceOf<Int64>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTips.[*].sortOrder", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTips.[*].title", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTips.[*].title", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTips.[*].description", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "additionalTips.[*].description", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "footer", Does.Contain("If you have any questions you can always call: 855-455-8777"));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetTipsWithToken", "footer", Is.Not.Empty);

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetTipsWithToken", Is.Not.EqualTo(400));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetTipsWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetTipsWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetTipsWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Tips Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetTipsWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportGetTipsWithoutToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPISupportUrl>.Time("SupportGetTipsWithoutToken", Is.LessThanOrEqualTo(3));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetTipsWithoutToken", Is.EqualTo(401));

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetTipsWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetTipsWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Tips Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetTipsAExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportGetTipsExpiredToken", Is.LessThanOrEqualTo(5));
                Step<MobileAPISupportUrl>.Time("SupportGetTipsExpiredToken", Is.LessThanOrEqualTo(3));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetTipsExpiredToken", Is.EqualTo(401));

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetTipsExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetTipsExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Faqs Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetFaqsWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportGetFAQsWithToken", Is.LessThanOrEqualTo(9000));
                Step<MobileAPISupportUrl>.Time("SupportGetFAQsWithToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetFAQsWithToken", Is.EqualTo(200));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "header", Does.Contain("We're here to help"));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "header", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "subHeader", Does.Contain("You got questions. We got answers."));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "subHeader", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].sortOrder", Is.All.InstanceOf<Int64>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].sortOrder", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].title", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].title", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].faQs.[*].sortOrder", Is.All.InstanceOf<Int64>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].faQs.[*].sortOrder", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].faQs.[*].question", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].faQs.[*].question", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].faQs.[*].answer", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "faqGroups.[*].faQs.[*].answer", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "footer", Does.Contain("Can't find your answer here? Just email us your question: myexperience@aarons.com"));
                Step<MobileAPISupportUrl>.ApiJson("SupportGetFAQsWithToken", "footer", Is.Not.Empty);

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportGetFAQsWithToken", Is.Not.EqualTo(400));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetFAQsWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetFAQsWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportGetFAQsWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Faqs Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetFaqsWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportFAQsWithoutToken", Is.LessThanOrEqualTo(9000));
                Step<MobileAPISupportUrl>.Time("SupportFAQsWithoutToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportFAQsWithoutToken", Is.EqualTo(401));

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportFAQsWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportFAQsWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Faqs Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetFaqsExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportFAQsExpiredToken", Is.LessThanOrEqualTo(9000));
                Step<MobileAPISupportUrl>.Time("SupportFAQsExpiredToken", Is.LessThanOrEqualTo(1));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportFAQsExpiredToken", Is.EqualTo(401));

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportFAQsExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportFAQsExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetContactWithToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportContactWithToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPISupportUrl>.Time("SupportContactWithToken", Is.LessThanOrEqualTo(25));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportContactWithToken", Is.EqualTo(200));
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "header", Does.Contain("Get in touch."));
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "header", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "subHeader", Does.Contain("We're here to answer your questions."));
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "subHeader", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "bodyContent", Does.Contain("Need Content Here?"));
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "bodyContent", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "chatURL", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "chatURL", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "chatEnabled", Is.All.InstanceOf<Boolean>());
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "chatEnabled", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "chatVisible", Is.All.InstanceOf<Boolean>());
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "chatVisible", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "supportPhone", Does.Contain("855-455-8777"));
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "supportPhone", Is.Not.Empty);
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "supportEmail", Does.Contain("myexperience@aarons.com"));
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "supportEmail", Is.Not.Empty);

                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "localStoreInfos.[*].sortOrder", Is.All.InstanceOf<Int64>());
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "localStoreInfos.[*].content", Is.All.InstanceOf<string>());
                Step<MobileAPISupportUrl>.ApiJson("SupportContactWithToken", "footer", Is.All.InstanceOf<string>());

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportContactWithToken", Is.Not.EqualTo(400));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportContactWithToken", Is.Not.EqualTo(HttpStatusCode.BadRequest));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportContactWithToken", Is.Not.EqualTo(HttpStatusCode.Forbidden));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportContactWithToken", Is.Not.EqualTo(HttpStatusCode.GatewayTimeout));
            });
        }

        [Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetContactWithoutToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportContactWithoutToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPISupportUrl>.Time("SupportContactWithoutToken", Is.LessThanOrEqualTo(15));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportContactWithoutToken", Is.EqualTo(401));

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportContactWithoutToken", Is.Not.EqualTo(200));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportContactWithoutToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }

        [Test, Description("Test Contact Screen Endpoint"), Author("Reginald Graham")]
        public void SupportGetContactExpiredToken()
        {
            Assert.Multiple(() =>
            {
                Step<MobileAPISupportUrl>.FileSize("SupportContactExpiredToken", Is.LessThanOrEqualTo(1000));
                Step<MobileAPISupportUrl>.Time("SupportContactExpiredToken", Is.LessThanOrEqualTo(100));
                Step<MobileAPISupportUrl>.StatusCode<int>("SupportContactExpiredToken", Is.EqualTo(401));

                Step<MobileAPISupportUrl>.StatusCode<int>("SupportContactExpiredToken", Is.Not.EqualTo(200));
                Step<MobileAPISupportUrl>.StatusCode<HttpStatusCode>("SupportContactExpiredToken", Is.Not.EqualTo(HttpStatusCode.OK));
            });
        }
    }
}