﻿using NUnit.Framework;
using NUnit.Framework.Constraints;
using System;

namespace API_Test_Framework.Extensions
{
    public static class Constraints
    {
        #region Address information
        /// <summary>
        /// <para>1. Is a String</para>
        /// <para>2. Not Empty</para>
        /// <para>3. Alphanumeric and can contain the special characters of . - # / and a space -- [a-zA-Z0-9 .\/#\-]+$</para>
        /// <paramref name="address1">Check against a specific address</paramref>/>
        /// </summary>
        public static Constraint Address1(this ConstraintExpression obj, String address1 = "")
        {
            if(address1 != "")
            {
                var addressStringMatch = Is.All.EqualTo(address1);
                return addressStringMatch;
            }

            var regexText = @"[a-zA-Z0-9 .\/#\-]+$";
            var isMatch = Is.All.Match(regexText).And.InstanceOf<string>().And.Not.Empty;
            return isMatch;
        }

        /// <summary>
        /// <para>1. Is a String</para>
        /// <para>2. Alphanumeric and can contain the special characters of . - # / and a space -- [a-zA-Z0-9 .\/#\-]+$</para>
        /// <para>3. Can be empty</para>
        /// <paramref name="address2">Check against a specific address</paramref>/>
        /// </summary>
        public static Constraint Address2(this ConstraintExpression obj, String address2 = "")
        {
            if (address2 != "")
            {
                var addressStringMatch = Is.All.EqualTo(address2);
                return addressStringMatch;
            }

            var regexText = @"[a-zA-Z0-9 .\/#\-]+$";
            var isMatch = Is.All.Match(regexText).And.InstanceOf<string>().Or.Empty;
            return isMatch;
        }

        /// <summary>
        /// <para>1. Is a String</para>
        /// <para>2. Not empty</para>
        /// <para>3. Is a-zA-Z and can contain a space</para>
        /// <paramref name="city">Check against a specific city</paramref>/>
        /// </summary>
        public static Constraint City(this ConstraintExpression obj, String city = "")
        {
            if (city != "")
            {
                var cityStringMatch = Is.All.EqualTo(city);
                return cityStringMatch;
            }

            var regexText = @"[a-zA-Z ]+$";
            var isMatch = Is.All.Match(regexText).And.InstanceOf<string>().And.Not.Empty;
            return isMatch;
        }

        /// <summary>
        /// <para>1. Two Uppercase Letters only</para>
        /// <paramref name="state">Check against a specific state</paramref>
        /// </summary>
        public static Constraint State(this ConstraintExpression obj, String state = "")
        {
            if (state != "")
            {
                var cityStateMatch = Is.All.EqualTo(state);
                return cityStateMatch;
            }

            var regexText = @"^[A-Z]{2}$";
            var isMatch = Is.All.Match(regexText);
            return isMatch;
        }

        /// <summary>
        /// 1. Defaults to US Zip code check: matches a digit (equal to [0-9])
        /// 2. Canadian Zip code: Alphanumeric 6 characters, with a space between each set of 3. Cannot contain D, F, I, O, Q, or U. Lastly, First position does not make use of the letters W or Z.
        /// </summary>
        /// <paramref name="isCanadian">Check against canadian zip format</paramref>
        /// <paramref name="zipcode">Check against a specific zip code</paramref>
        /// <returns></returns>
        public static Constraint Zipcode(this ConstraintExpression obj, bool isCanadian = false, String zipcode = "")
        {
            if (zipcode != "")
            {
                var zipCodeMatch = Is.All.EqualTo(zipcode);
                return zipCodeMatch;
            }

            var regexText = "";

            if(isCanadian)
            {
                regexText = @"(^\d{5}$)|(^([a-ceghj-npr-tvxyA-CEGHJ-NPR-TVXY]\d[a-ceghj-npr-tv-zA-CEGHJ-NPR-TV-Z])[ -]{0,1}(\d[a-ceghj-npr-tv-zA-CEGHJ-NPR-TV-Z]\d)$)";
                var isMatchCanadian = Is.All.Match(regexText);
                return isMatchCanadian;
            }

            regexText = @"^\d{5}(?:[-\s]\d{4})?$";
            var isMatchUS = Is.All.Match(regexText);
            return isMatchUS;
        }
        #endregion

    }
}
