﻿using AutoFrameWork.Core;
using NUnit.Framework;

namespace API_Test_Framework
{
    internal class EnvtConfigurator
    {
        static EnvtConfigurator()
        {
        }

        private string newURL;
        private static string envtStaging = "staging";
        private static string envtNonProd = "np";
        private static string envtProd = "prod";

        public string envtSetup(string apiUrl)
        {
            var apiEnvironment = TestContext.Parameters["envtUrl"];

            // If environment parameter == null, default to np environment
            if (apiEnvironment == null)
            {
                if (apiUrl == "Account")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIAccountUrl.SupplyValue();
                }
                else if (apiUrl == "EZ_Pay")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue();
                }
                else if (apiUrl == "Home")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIHomeUrl.SupplyValue();
                }
                else if (apiUrl == "On_Boarding")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIOnBoardingUrl.SupplyValue();
                }
                else if (apiUrl == "Support")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPISupportUrl.SupplyValue();
                }
            }

            // If environment parameter == staging, use stating environment
            else if (apiEnvironment == envtStaging)
            {
                if (apiUrl == "Account")
                {
                    newURL = Settings.Setting.StagingEnvironments.MobileAPIAccountStagingUrl.SupplyValue();
                }
                else if (apiUrl == "EZ_Pay")
                {
                    newURL = Settings.Setting.StagingEnvironments.MobileAPIEZPayStagingUrl.SupplyValue();
                }
                else if (apiUrl == "Home")
                {
                    newURL = Settings.Setting.StagingEnvironments.MobileAPIHomeStagingUrl.SupplyValue();
                }
                else if (apiUrl == "On_Boarding")
                {
                    newURL = Settings.Setting.StagingEnvironments.MobileAPIOnBoardingStagingUrl.SupplyValue();
                }
                else if (apiUrl == "Support")
                {
                    newURL = Settings.Setting.StagingEnvironments.MobileAPISupportStagingUrl.SupplyValue();
                }
            }

            // If environment parameter == prod, use production environment
            else if (apiEnvironment == envtProd)
            {
                if (apiUrl == "Account")
                {
                    newURL = Settings.Setting.ProdEnvironments.MobileAPIAccountProdUrl.SupplyValue();
                }
                else if (apiUrl == "EZ_Pay")
                {
                    newURL = Settings.Setting.ProdEnvironments.MobileAPIEZPayProdUrl.SupplyValue();
                }
                else if (apiUrl == "Home")
                {
                    newURL = Settings.Setting.ProdEnvironments.MobileAPIHomeProdUrl.SupplyValue();
                }
                else if (apiUrl == "On_Boarding")
                {
                    newURL = Settings.Setting.ProdEnvironments.MobileAPIOnBoardingProdUrl.SupplyValue();
                }
                else if (apiUrl == "Support")
                {
                    newURL = Settings.Setting.ProdEnvironments.MobileAPISupportProdUrl.SupplyValue();
                }
            }

            // If environment parameter == np, use non-production environment
            else if (apiEnvironment == envtNonProd)
            {
                if (apiUrl == "Account")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIAccountUrl.SupplyValue();
                }
                else if (apiUrl == "EZ_Pay")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue();
                }
                else if (apiUrl == "Home")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIHomeUrl.SupplyValue();
                }
                else if (apiUrl == "On_Boarding")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPIOnBoardingUrl.SupplyValue();
                }
                else if (apiUrl == "Support")
                {
                    newURL = Settings.Setting.NonProdEnvironments.MobileAPISupportUrl.SupplyValue();
                }
            }

            return newURL;
        }
    }
}