﻿using AutoFrameWork.API;
using AutoFrameWork.Core;
using RestSharp;
using System;
using System.IO;

namespace API_Test_Framework.Apis
{
    public class IntegrationUrl
    {
        private static ApiBuilder _api;

        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();

        private static string aaronsTestuser01UserName = "aaronstestuser01@gmail.com";
        private static string aaronsTestuser01Password = "test123";
        private static string aaronsTestUser01UserID = "auth0|5af2f1d8e1fee0667008fe30";
        private static int aaronsTestUser01GlobalCustomerID = 200;
        private static string aaronsTestUser01CardID = "54321";
        private static string aaronsTestUser01BillingZip = "30301";
        private static string aaronsTestUser01CVV = "321";
        private static int aaronsTestUser01ExpirationMonth = 05;
        private static int aaronsTestUser01ExpirationYear = 2020;

        public static string getAaronsTestUser01UserName()
        {
            return aaronsTestuser01UserName;
        }

        public static string getAaronsTestUser01Password()
        {
            return aaronsTestuser01Password;
        }

        public static string getTestUser01UserID()
        {
            return aaronsTestUser01UserID;
        }

        public static int getTestUser01GlobalCustomerID()
        {
            return aaronsTestUser01GlobalCustomerID;
        }

        public static string getTestUser01CardID()
        {
            return aaronsTestUser01CardID;
        }

        public static string getTestUser01BillingZip()
        {
            return aaronsTestUser01BillingZip;
        }

        public static string getTestUser01CVV()
        {
            return aaronsTestUser01CVV;
        }

        public static int getTestUser01ExpirationMonth()
        {
            return aaronsTestUser01ExpirationMonth;
        }

        public static int getTestUser01ExpirationYear()
        {
            return aaronsTestUser01ExpirationYear;
        }

        static IntegrationUrl()
        {
            _api = new ApiBuilder();
        }

        public static IApiBuilderStats MobileAPIEZPayGetAllCardsWithForSpecificUser =>
            _api.Name("Get all credit cards for the use")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue())
                .Resource("/card")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats MobileAPIEZAPIDetermineIfUserAllowedToSetupEZPayForSpecificUser =>
           _api.Name("Get all credit cards for the use")
               .Use(Method.GET)
               .ReturnAs(DataFormat.Json)
               .UseUrl(Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue())
               .Resource("/ezpay/getcansetupezpay")
               .AddHeader("Accept", "application/json")
               .AddHeader("Content-Type", "application/json")
               .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
               .SendAs(DataFormat.Json)
               .Execute();

        public static IApiBuilderStats MobileAPIOnBoardingGetAgreementsSpecificUser =>
            _api.Name("Mobile API Onboarding Get Agreements")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .AddHeader("CustomerId", "101")
                .AddHeader("ProductId", "1")
                .AddHeader("Accept", "application/json")
                .Resource("/onboarding/agreements?CustomerId=101&ProductId=1")
                .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats MobileAPIEZPayGetScheduleSpecificUser =>
            _api.Name("Mobile API Get EZPay Schedule")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue())
                .Resource("/ezpay/schedule")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats MobileAPIEZPayPostOneTimePaymentDetailsForHPPSessionSpecificUser =>
            _api.Name("Post one time payment details for HPP session")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue())
                .Resource("/ezpay/onetimepayment")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\EZPay\EZPayOneTimePaymentDetailsHPP.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats MobileAPIEZPayCreatesOneTimePaymentWithToken =>
            _api.Name("Add card for HPP Session Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue())
                .Resource("/ezpay/addonetimepaymentconfirm")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\EZPay\OneTimePaymentConfirm.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0UpdateUserSpecificUser =>
            _api.Name("Auth0 Update User")
                .Use(Method.PATCH)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/" + getTestUser01UserID())
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + authOB.GetManagementAccessToken())
                .AddParameter("application/json", "{ " + "\"user_metadata\"" + ": " + "{" + "\"GlobalCustomerId\"" + ":" + IntegrationUrl.getTestUser01GlobalCustomerID() + "}" + " }", ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats MobileAPIEZPayUpdateCardSpecificUser =>
            _api.Name("Update a user's credit card")
                .Use(Method.PATCH)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.MobileAPIEZPayUrl.SupplyValue())
                .Resource("/card?CardId=12345&BillingZip=30101&CVV=123&ExpirationMonth=01&ExpirationYear=2020")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddParameter("CardId", aaronsTestUser01CardID)
                .AddParameter("BillingZip", getTestUser01CardID())
                .AddParameter("CVV", getTestUser01BillingZip())
                .AddParameter("ExpirationMonth", getTestUser01ExpirationMonth())
                .AddParameter("ExpirationYear", getTestUser01ExpirationYear())
                .AddHeader("Authorization", "Bearer " + authOB.GetSpecificUserAccessToken(IntegrationUrl.getAaronsTestUser01UserName(), IntegrationUrl.getAaronsTestUser01Password()))
                .SendAs(DataFormat.Json)
                .Execute();
    }
}