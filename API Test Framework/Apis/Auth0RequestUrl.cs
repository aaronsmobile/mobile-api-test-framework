﻿using AutoFrameWork.API;
using AutoFrameWork.Core;
using RestSharp;
using System;
using System.IO;

namespace API_Test_Framework.Apis
{
    public class Auth0RequestUrl
    {
        private static ApiBuilder _api;

        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();

        static Auth0RequestUrl()
        {
            _api = new ApiBuilder();
        }

        public static IApiBuilderStats Auth0LoginRequest =>
            _api.Name("Auth0 Login Request")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Accept", "*/*")
                .Resource("/oauth/token")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\LoginRequest.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0SignUpRequest =>
            _api.Name("Auth0 Sign Up Request")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .AddHeader("Content-Type", "application/json")
                .Resource("/dbconnections/signup")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\Auth0SignUp.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0SearchUsersWithToken =>
            _api.Name("Auth0 Search Users")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users-by-email?fields=user_id,email_verified,user_metadata&include_fields=true&email=reginald.graham@aarons.com")
                .AddHeader("Authorization", "Bearer " + authOB.GetManagementAccessToken())
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0SearchUsersWithoutToken =>
            _api.Name("Auth0 Search Users")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users-by-email?fields=user_id,email_verified,user_metadata&include_fields=true&email=reginald.graham@aarons.com")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0SearchUsersExpiredToken =>
            _api.Name("Auth0 Search Users")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users-by-email?fields=user_id,email_verified,user_metadata&include_fields=true&email=reginald.graham@aarons.com")
                .AddHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0DeleteUserWithToken =>
            _api.Name("Auth0 Delete User")
                .Use(Method.DELETE)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/auth0|5ab3f7ccecd90e02632513cb")
                .AddHeader("Authorization", Settings.Setting.NonProdEnvironments.AccessBearerToken.SupplyValue())
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0DeleteUserWithoutToken =>
            _api.Name("Auth0 Delete User")
                .Use(Method.DELETE)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/auth0|5ab3f7ccecd90e02632513cb")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0DeleteUserExpiredToken =>
            _api.Name("Auth0 Delete User")
                .Use(Method.DELETE)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/auth0|5ab3f7ccecd90e02632513cb")
                .AddHeader("Authorization", "")
                .SendAs(DataFormat.Json)
                .AddHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg")
                .Execute();

        public static IApiBuilderStats Auth0UpdateUserWithToken =>
            _api.Name("Auth0 Update User")
                .Use(Method.PATCH)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/auth0|5ada23bf5fb3bc1b6d7b08a3")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + authOB.GetManagementAccessToken())
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\PatchUpdateUser.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0UpdateUserWithoutToken =>
            _api.Name("Auth0 Update User")
                .Use(Method.PATCH)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/auth0|5ada23bf5fb3bc1b6d7b08a3")
                .AddHeader("Content-Type", "application/json")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\PatchUpdateUser.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0UpdateUserExpiredToken =>
            _api.Name("Auth0 Update User")
                .Use(Method.PATCH)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/api/v2/users/auth0|5ada23bf5fb3bc1b6d7b08a3")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\PatchUpdateUser.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0GetManagementAPIWithToken =>
            _api.Name("Auth0 Get Management Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/oauth/token")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", Settings.Setting.NonProdEnvironments.AccessBearerToken.SupplyValue())
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\GetManagementToken.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0GetManagementAPIWithoutToken =>
            _api.Name("Auth0 Get Management Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/oauth/token")
                .AddHeader("Content-Type", "application/application/json")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\GetManagementToken.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0GetManagementAPIExpiredToken =>
            _api.Name("Auth0 Get Management Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/oauth/token")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\GetManagementToken.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0RefreshWithToken =>
            _api.Name("Auth0 Refresh Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/oauth/token")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Accept", "*/*")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\RefreshToken.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0RefreshWithoutToken =>
            _api.Name("Auth0 Refresh Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/oauth/token")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Accept", "*/*").AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\RefreshToken.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats Auth0RefreshExpiredToken =>
            _api.Name("Auth0 Refresh Token")
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.AaronsAuth0Url.SupplyValue())
                .Resource("/oauth/token")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Accept", "*/*").AddHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg")
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"\Test Data\Auth0\RefreshToken.txt"), ParameterType.RequestBody)
                .SendAs(DataFormat.Json)
                .Execute();
    }
}