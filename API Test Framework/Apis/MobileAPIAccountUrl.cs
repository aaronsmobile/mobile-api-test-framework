﻿using AutoFrameWork.API;
using RestSharp;

namespace API_Test_Framework.Apis
{
    public class MobileAPIAccountUrl
    {
        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();
        private static EnvtConfigurator envt = new EnvtConfigurator();

        private static ApiBuilder _api;

        static MobileAPIAccountUrl()
        {
            _api = new ApiBuilder();
        }

        private static string aaronsTestuser01UserName = "aaronstestuser01+23@gmail.com";
        private static string aaronsTestuser01Password = "@aronsTest123";
        private static string accessToken { get; set; }

        public string GetAccessToken()
        {
            string userToken = authOB.GetSpecificUserAccessToken(MobileAPIAccountUrl.getAaronsTestUser01UserName(), MobileAPIAccountUrl.getAaronsTestUser01Password());

            accessToken = userToken;

            return accessToken;
        }

        public static string getAaronsTestUser01UserName()
        {
            return aaronsTestuser01UserName;
        }

        public static string getAaronsTestUser01Password()
        {
            return aaronsTestuser01Password;
        }

        public static IApiBuilderStats AccountPaymentHistoryWithToken
        {
            get
            {
                var result = GETMethod("AccountPaymentHistoryWithToken", "/Payment/GetPaymentHistoryRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountPaymentHistoryWithoutToken
        {
            get
            {
                var result = GETMethod("AccountPaymentHistoryWithoutToken", "/Payment/GetPaymentHistoryRequest?format=json&stubParams=agreementcount:2;agreementpaymentcount:2;payout:1;agreementage:10", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountPaymentHistoryExpiredToken
        {
            get
            {
                var result = GETMethod("AccountPaymentHistoryWithoutToken", "/Payment/GetPaymentHistoryRequest?format=json&stubParams=agreementcount:2;agreementpaymentcount:2;payout:1;agreementage:10", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountPaymentHistoryNoSubscription
        {
            get
            {
                var result = GETMethod("AccountPaymentHistoryNoSubscription", "/Payment/GetPaymentHistoryRequest?format=json&stubParams=agreementcount:2;agreementpaymentcount:2;payout:1;agreementage:10", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetPaymentDetailWithToken
        {
            get
            {
                var result = GETMethod("AccountGetPaymentDetailWithToken", "/Payment/GetPaymentDetailRequest?PaymentId=1&Store=SMALL&format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetPaymentDetailWithoutToken
        {
            get
            {
                var result = GETMethod("AccountGetPaymentDetailWithoutToken", "/Payment/GetPaymentDetailRequest?PaymentId=1&Store=SMALL&format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetPaymentDetailsExpiredToken
        {
            get
            {
                var result = GETMethod("AccountGetPaymentDetailsExpiredToken", "/Payment/GetPaymentDetailRequest?PaymentId=1&Store=SMALL&format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetPaymentDetailNoSubscription
        {
            get
            {
                var result = GETMethod("AccountGetPaymentDetailNoSubscription", "/Payment/GetPaymentDetailRequest?PaymentId=1&Store=SMALL&format=json", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetProfileWithToken
        {
            get
            {
                var result = GETMethod("AccountGetProfileWithToken", "/Profile/GetUserProfileRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetProfileWithoutToken
        {
            get
            {
                var result = GETMethod("AccountGetProfileWithoutToken", "/Profile/GetUserProfileRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetProfileWithExpiredToken
        {
            get
            {
                var result = GETMethod("AccountGetProfileWithExpiredToken", "/Profile/GetUserProfileRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountGetProfileNoSubscription
        {
            get
            {
                var result = PATCHMethod("AccountGetProfileNoSubscription", "/Profile/GetUserProfileRequest?format=json", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotifcationTrueWithToken
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotifcationTrueWithToken", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=true&format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats UpdatePushNotifcationTrueNoSubscription
        {
            get
            {
                var result = PATCHMethod("UpdatePushNotifcationTrueNoSubscription", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=true&format=json", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotifcationFalseWithToken
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotifcationFalseWithToken", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=false&format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotifcationFalseNoSubscription
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotifcationFalseNoSubscription", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=false&format=json", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotificationTrueWithoutToken
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotificationTrueWithoutToken", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=false&format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotificationFalseWithoutToken
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotificationFalseWithoutToken", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=false&format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotificationTrueWithExpiredToken
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotificationTrueWithExpiredToken", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=false&format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats AccountUpdatePushNotificationFalseWithExpiredToken
        {
            get
            {
                var result = PATCHMethod("AccountUpdatePushNotificationFalseWithExpiredToken", "/Profile/UpdatePushNotificationRequest?NotificationType=0&Enabled=false&format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats PATCHMethod(string Name, string resource, string subscriptionNumber = "", string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.PATCH)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("Account"))
                .Resource(resource)
                .AddHeader("Ocp-Apim-Subscription-Key", subscriptionNumber)
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .SendAs(DataFormat.Json);

            return result;
        }

        public static IApiBuilderStats GETMethod(string Name, string resource, string subscriptionNumber = "", string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("Account"))
                .Resource(resource)
                .AddHeader("Ocp-Apim-Subscription-Key", subscriptionNumber)
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .SendAs(DataFormat.Json);

            return result;
        }
    }
}