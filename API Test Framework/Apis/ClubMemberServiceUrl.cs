﻿using AutoFrameWork.API;
using AutoFrameWork.Core;
using RestSharp;

namespace API_Test_Framework.Apis
{
    public class ClubMemberServiceUrl
    {
        private static ApiBuilder _api;

        static ClubMemberServiceUrl()
        {
            _api = new ApiBuilder();
        }

        public static IApiBuilderStats ClubMembershipServiceSearchMembershipsActive =>
            _api.Name("Search Active Club Memberships")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.ClubMembershipServiceUrl.SupplyValue())
                .Resource("/v2/membershiplist?MaxResults=10&MembershipStatus=active")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats ClubMembershipServiceGetMembershipsActive =>
            _api.Name("Get Active Club Memberships")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.ClubMembershipServiceUrl.SupplyValue())
                .Resource("/v2/membership?servicingStore=C0242&storeOpsPersonId=4338")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats ClubMembershipServiceReccurencePatterns =>
            _api.Name("Club membership Reccurence Patterns")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.ClubMembershipServiceUrl.SupplyValue())
                .Resource("/v2/recurrencePatterns?store=C0372&storeOpsPersonId=27436")
                .SendAs(DataFormat.Json)
                .Execute();
    }
}