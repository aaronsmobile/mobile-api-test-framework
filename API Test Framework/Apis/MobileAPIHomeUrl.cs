﻿using AutoFrameWork.API;
using RestSharp;
using System;
using System.IO;

namespace API_Test_Framework.Apis
{
    public class MobileAPIHomeUrl
    {
        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();
        private static EnvtConfigurator envt = new EnvtConfigurator();

        private static ApiBuilder _api;

        static MobileAPIHomeUrl()
        {
            _api = new ApiBuilder();
        }

        private static string accessToken { get; set; }

        public string GetAccessToken()
        {
            string userToken = authOB.GetAccessToken();

            accessToken = userToken;

            return accessToken;
        }

        public static IApiBuilderStats HomeGetCustomerAgreementDetailsWithToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerAgreementDetailsWithToken", "/agreement/details/C1881/1?&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:1;paymentsCompleted:33;nextPaidThroughDate:07/01/2018;leaseFrequency:Monthly;sameAsCashDuration:40", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetCustomerAgreementDetailsWithoutToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerAgreementDetailsWithoutToken", "/agreement/details/C1881/1?&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:1;paymentsCompleted:33;nextPaidThroughDate:07/01/2018;leaseFrequency:Monthly;sameAsCashDuration:40", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetCustomerAgreementDetailsExpiredToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerAgreementDetailsExpiredToken", "/agreement/details/C1881/1?&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:1;paymentsCompleted:33;nextPaidThroughDate:07/01/2018;leaseFrequency:Monthly;sameAsCashDuration:40", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetCustomerAgreementDetailsInactiveAgreementToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerAgreementDetailsInactiveAgreementToken", "/agreement/details/C1881/1?&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:1;paymentsCompleted:33;nextPaidThroughDate:07/01/2018;leaseFrequency:Monthly;sameAsCashDuration:40;payout:1", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetCustomerSummaryRequestWithToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerSummaryRequestWithToken", "/customer/summary?useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;payout:1;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:123", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetCustomerSummaryRequestWithoutToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerSummaryRequestWithoutToken", "/customer/summary?useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;payout:1;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:123", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetCustomerSummaryRequestExpiredToken
        {
            get
            {
                var result = GETMethod("HomeGetCustomerSummaryRequestExpiredToken", "/customer/summary?useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;payout:1;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:123", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetPartialPayOptionsRequestWithToken
        {
            get
            {
                var result = GETMethod("HomeGetPartialPayOptionsRequestWithToken", "/payment/partialpayoptions", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetPartialPayOptionsRequestWithoutToken
        {
            get
            {
                var result = GETMethod("HomeGetPartialPayOptionsRequestWithoutToken", "/payment/partialpayoptions", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetPartialPayOptionsRequestExpiredToken
        {
            get
            {
                var result = GETMethod("HomeGetPartialPayOptionsRequestExpiredToken", "/payment/partialpayoptions", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeAddPartialPaymentRequestWithToken
        {
            get
            {
                var result = GETMethod("HomeAddPartialPaymentRequestWithToken", "/payment/partialpayoptions", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeAddPartialPaymentRequestWithoutToken
        {
            get
            {
                var result = GETMethod("HomeAddPartialPaymentRequestWithoutToken", "/payment/partialpayoptions", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeAddPartialPaymentRequestExpiredToken
        {
            get
            {
                var result = GETMethod("HomeAddPartialPaymentRequestExpiredToken", "/payment/partialpayoptions", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeAddPromiseToPayWithToken
        {
            get
            {
                var result = POSTMethod("HomeAddPromiseToPayWithToken", "/payment/promisetopay", @"\Test Data\Home\AddPromiseToPay.txt", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeAddPromiseToPayWithoutToken
        {
            get
            {
                var result = POSTMethod("HomeAddPromiseToPayWithoutToken", "/payment/promisetopay", @"\Test Data\Home\AddPromiseToPay.txt", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeAddPromiseToPayExpiredToken
        {
            get
            {
                var result = POSTMethod("HomeAddPromiseToPayExpiredToken", "/payment/promisetopay", @"\Test Data\Home\AddPromiseToPay.txt", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetPromiseToPayOptionsRequestWithToken
        {
            get
            {
                var result = GETMethod("HomeGetPromiseToPayOptionsRequestWithToken", "/payment/promisetopayoptions", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetPromiseToPayOptionsRequestWithoutToken
        {
            get
            {
                var result = GETMethod("HomeGetPromiseToPayOptionsRequestWithoutToken", "/payment/promisetopayoptions", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats HomeGetPromiseToPayOptionsRequestExpiredToken
        {
            get
            {
                var result = GETMethod("HomeGetPromiseToPayOptionsRequestExpiredToken", "/payment/promisetopayoptions", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats POSTMethod(string Name, string resource, string testData, string subscriptionNumber = "", string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("Home"))
                .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .AddHeader("Ocp-Apim-Subscription-Key", subscriptionNumber)
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + testData), ParameterType.RequestBody)
                .SendAs(DataFormat.Json);

            return result;
        }

        public static IApiBuilderStats GETMethod(string Name, string resource, string subscriptionNumber = "", string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("Home"))
                .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .AddHeader("Ocp-Apim-Subscription-Key", subscriptionNumber)
                .SendAs(DataFormat.Json);

            return result;
        }
    }
}