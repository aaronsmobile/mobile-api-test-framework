﻿using AutoFrameWork.API;
using RestSharp;
using System;
using System.IO;

namespace API_Test_Framework.Apis
{
    public class MobileAPIOnBoardingUrl
    {
        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();
        private static EnvtConfigurator envt = new EnvtConfigurator();

        private static ApiBuilder _api;
        private static string previousDayTimeWithin24Hours = DateTime.Now.AddDays(-1).AddHours(+5).ToString();
        private static string previousDayTimeOutside24Hours = DateTime.Now.AddDays(-1).AddHours(-8).ToString();

        static MobileAPIOnBoardingUrl()
        {
            _api = new ApiBuilder();
        }

        private static string accessToken { get; set; }

        public string GetAccessToken()
        {
            string userToken = authOB.GetAccessToken();

            accessToken = userToken;

            return accessToken;
        }

        public static IApiBuilderStats OnBoardingCanUserLoginWithToken
        {
            get
            {
                var result = GETMethod("OnBoardingCanUserLoginWithToken", "/onboarding/canUserLogin?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingCanUserLoginWithoutToken
        {
            get
            {
                var result = GETMethod("OnBoardingCanUserLoginWithoutToken", "/onboarding/canUserLogin?format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingCanUserLoginExpiredToken
        {
            get
            {
                var result = GETMethod("OnBoardingCanUserLoginExpiredToken", "/onboarding/canUserLogin?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingFindCustomerWithToken
        {
            get
            {
                var result = GETMethod("OnBoardingFindCustomerWithToken", "/onboarding/findCustomer?lastName=aleman&postalCode=30102&lastFourSsn=0330&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:123", "", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingFindCustomerWithoutToken
        {
            get
            {
                var result = GETMethod("OnBoardingFindCustomerWithoutToken", "/onboarding/findCustomer?lastName=aleman&postalCode=30102&lastFourSsn=0330&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:123").Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingFindCustomerExpiredToken
        {
            get
            {
                var result = GETMethod("OnBoardingFindCustomerExpiredToken", "/onboarding/findCustomer?lastName=aleman&postalCode=30102&lastFourSsn=0330&useStub=true&stubParams=agreementcount:5;agreementPaymentCount:3;agreementage:10;mastercustomername:constantine;clubmembershipstatus:cancel;receiptsize:LARGE;userEmailVerified:true;userCreatedAt:06/15/2018 9:00am;sourcestore:C1881;agreementId:123", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingSetCustomerIdWithToken
        {
            get
            {
                var result = POSTMethod("OnBoardingSetCustomerIdWithToken", "/onboarding/setCustomerId?format=json", @"\Test Data\OnBoarding\OnBoardingSetCustomerId.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingSetCustomerIdWithoutToken
        {
            get
            {
                var result = POSTMethod("OnBoardingSetCustomerIdWithoutToken", "/onboarding/setCustomerId?format=json", @"\Test Data\OnBoarding\OnBoardingSetCustomerId.txt").Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingSetCustomerIdExpiredToken
        {
            get
            {
                var result = POSTMethod("OnBoardingSetCustomerIdExpiredToken", "/onboarding/setCustomerId?format=json", @"\Test Data\OnBoarding\OnBoardingSetCustomerId.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingcanUserLoginuseInvalidTrue
        {
            get
            {
                var result = GETMethod("OnBoardingcanUserLoginuseInvalidTrue", "/onboarding/canUserLogin?useStub=true&stubParams=userInvalid:true", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingcanUserLoginuseInvalidFalse
        {
            get
            {
                var result = GETMethod("OnBoardingcanUserLoginuseInvalidFalse", "/onboarding/canUserLogin?useStub=false&stubParams=userInvalid:false", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingUserCreatedAtYesterdayOutside24HourPeriod
        {
            get
            {
                var result = GETMethod("OnBoardingUserCreatedAtYesterdayOutside24HourPeriod", "/onboarding/canUserLogin?useStub=true&stubParams=userCreatedAt:", previousDayTimeOutside24Hours, accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats OnBoardingUserCreatedAtYesterdayWithin24HourPeriod
        {
            get
            {
                var result = GETMethod("OnBoardingUserCreatedAtYesterdayWithin24HourPeriod", "/onboarding/canUserLogin?useStub=true&stubParams=userCreatedAt:", previousDayTimeWithin24Hours, accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats POSTMethod(string Name, string resource, string testData, string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("On_Boarding"))
                .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + testData), ParameterType.RequestBody)
                .SendAs(DataFormat.Json);

            return result;
        }

        public static IApiBuilderStats GETMethod(string Name, string resource, string resourceTime = "", string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("On_Boarding"))
                .Resource(resource + resourceTime)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .SendAs(DataFormat.Json);

            return result;
        }
    }
}