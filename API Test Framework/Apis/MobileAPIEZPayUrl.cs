﻿using AutoFrameWork.API;
using RestSharp;
using System;
using System.IO;

namespace API_Test_Framework.Apis
{
    public class MobileAPIEZPayUrl
    {
        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();
        private static EnvtConfigurator envt = new EnvtConfigurator();

        private static ApiBuilder _api;

        static MobileAPIEZPayUrl()
        {
            _api = new ApiBuilder();
        }

        private static string aaronsTestuser01UserName = "aaronstestuser01+24@gmail.com";
        private static string aaronsTestuser01Password = "test123";
        private static string accessToken { get; set; }

        public string GetAccessToken()
        {
            string userToken = authOB.GetSpecificUserAccessToken(MobileAPIAccountUrl.getAaronsTestUser01UserName(), MobileAPIAccountUrl.getAaronsTestUser01Password());

            accessToken = userToken;

            return accessToken;
        }

        public static string getAaronsTestUser01UserName()
        {
            return aaronsTestuser01UserName;
        }

        public static string getAaronsTestUser01Password()
        {
            return aaronsTestuser01Password;
        }

        public static IApiBuilderStats EZPayGetAllCardWithToken
        {
            get
            {
                var result = GETMethod("EZPayGetAllCardWithToken", "/card", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetAllCardWithoutToken
        {
            get
            {
                var result = GETMethod("EZPayGetAllCardWithoutToken", "/card").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetAllCardExpiredToken
        {
            get
            {
                var result = GETMethod("EZPayGetAllCardExpiredToken", "/card", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej - XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ - JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0 - kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5 - SdAdIGpn1kdW3IcDAm - JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF - HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayDeactivateCardWithToken
        {
            get
            {
                var result = DELETEMethod("EZPayDeactivateCardWithToken", "/card?CardId=9c51fdbf-5b34-48e7-a5f7-41e39713aadc&useStub=true&stubParams=stubDeleteCard:true", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayDeactivateCardWithoutToken
        {
            get
            {
                var result = DELETEMethod("EZPayDeactivateCardWithoutToken", "/card?CardId=9c51fdbf-5b34-48e7-a5f7-41e39713aadc&useStub=true&stubParams=stubDeleteCard:true").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayDeactivateCardWithExpiredToken
        {
            get
            {
                var result = DELETEMethod("EZPayDeactivateCardWithExpiredToken", "/card?CardId=9c51fdbf-5b34-48e7-a5f7-41e39713aadc&useStub=true&stubParams=stubDeleteCard:true", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayAddCardForHPPWithToken
        {
            get
            {
                var result = POSTMethod("EZPayAddCardForHPPWithToken", "/card", @"\Test Data\EZPay\EZPayHPP.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayAddCardForHPPWithoutToken
        {
            get
            {
                var result = POSTMethod("EZPayAddCardForHPPWithoutToken", "/card", @"\Test Data\EZPay\EZPayHPP.txt").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayAddCardForHPPExpiredToken
        {
            get
            {
                var result = POSTMethod("EZPayAddCardForHPPExpiredToken", "/card", @"\Test Data\EZPay\EZPayHPP.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken
        {
            get
            {
                var result = GETMethod("EZPayDetermineWhetherUserAllowedToSetupEZPayWithToken", "/ezpay/getcansetupezpay", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken
        {
            get
            {
                var result = GETMethod("EZPayDetermineWhetherUserAllowedToSetupEZPayWithoutToken", "/ezpay/getcansetupezpay").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayDetermineWhetherUserAllowedToSetupEZPayExpiredToken
        {
            get
            {
                var result = POSTMethod("EZPayEstablishEZPayScheduleWithToken", "/ezpay/schedule", @"\Test Data\EZPay\EZPayPostSchedule.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayEstablishEZPayScheduleWithToken
        {
            get
            {
                var result = POSTMethod("EZPayEstablishEZPayScheduleWithToken", "/ezpay/schedule", @"\Test Data\EZPay\EZPayPostSchedule.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayEstablishEZPayScheduleWithoutToken
        {
            get
            {
                var result = POSTMethod("EZPayEstablishEZPayScheduleWithoutToken", "/ezpay/schedule", @"\Test Data\EZPay\EZPayPostSchedule.txt").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayEstablishEZPayScheduleExpiredToken
        {
            get
            {
                var result = POSTMethod("EZPayEstablishEZPayScheduleExpiredToken", "/ezpay/schedule", @"\Test Data\EZPay\EZPayPostSchedule.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej - XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ - JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0 - kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5 - SdAdIGpn1kdW3IcDAm - JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF - HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPaySetupOneTimePaymentDetailsWithToken
        {
            get
            {
                var result = GETMethod("EZPaySetupOneTimePaymentDetailsWithToken", "/ezpay/setuponetimepaymentdetails", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPaySetupOneTimePaymentDetailsWithoutToken
        {
            get
            {
                var result = GETMethod("EZPaySetupOneTimePaymentDetailsWithoutToken", "/ezpay/setuponetimepaymentdetails").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPaySetupOneTimePaymentDetailsExpiredToken
        {
            get
            {
                var result = GETMethod("EZPaySetupOneTimePaymentDetailsExpiredToken", "/ezpay/setuponetimepaymentdetails", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetDetailsForSettingUpEZPayWithToken
        {
            get
            {
                var result = GETMethod("EZPayGetDetailsForSettingUpEZPayWithToken", "/ezpay/setupezpaydetails?recurrenceId=4&recurrenceFirstValue=12&recurrenceSecondValue=15", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetDetailsForSettingUpEZPayWithoutToken
        {
            get
            {
                var result = GETMethod("EZPayGetDetailsForSettingUpEZPayWithoutToken", "/ezpay/setupezpaydetails?recurrenceId=8&recurrenceFirstValue=6&recurrenceSecondValue=7").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetDetailsForSettingUpEZPayWithExpired
        {
            get
            {
                var result = GETMethod("EZPayGetDetailsForSettingUpEZPayWithExpired", "/ezpay/setupezpaydetails?recurrenceId=8&recurrenceFirstValue=6&recurrenceSecondValue=7", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetScheduleWithToken
        {
            get
            {
                var result = GETMethod("EZPayGetScheduleWithToken", "/ezpay/schedule", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetScheduleWithoutToken
        {
            get
            {
                var result = GETMethod("EZPayGetScheduleWithoutToken", "/ezpay/schedule").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayGetScheduleExpiredToken
        {
            get
            {
                var result = GETMethod("EZPayGetScheduleWithoutToken", "/ezpay/schedule", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneTimeFuturePaymentWithToken
        {
            get
            {
                var result = POSTMethod("EZPayPostOneTimeFuturePaymentWithToken", "/ezpay/onetimefuturepayment", @"\Test Data\EZPay\EZPayOneTimeFuturePayment.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneTimeFuturePaymentWithoutToken
        {
            get
            {
                var result = POSTMethod("EZPayPostOneTimeFuturePaymentWithoutToken", "/ezpay/onetimefuturepayment", @"\Test Data\EZPay\EZPayOneTimeFuturePayment.txt").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneTimeFuturePaymentExpiredToken
        {
            get
            {
                var result = POSTMethod("EZPayPostOneTimeFuturePaymentExpiredToken", "/ezpay/onetimefuturepayment", @"\Test Data\EZPay\EZPayOneTimeFuturePayment.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneTimeFuturePaymentNoContentType
        {
            get
            {
                var result = POSTMethod("EZPayPostOneTimeFuturePaymentNoContentType", "/ezpay/onetimefuturepayment", @"\Test Data\EZPay\EZPayOneTimeFuturePayment.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayCreatesOneTimePaymentImmediatePaymentWithToken
        {
            get
            {
                var result = POSTMethod("EZPayCreatesOneTimePaymentImmediatePaymentWithToken", "/ezpay/onetimeimmediatepayment", @"\Test Data\EZPay\EZPayOneTimeImmediatePayment.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken
        {
            get
            {
                var result = POSTMethod("EZPayCreatesOneTimePaymentImmediatePaymentWithoutToken", "/ezpay/onetimeimmediatepayment", @"\Test Data\EZPay\EZPayOneTimeImmediatePayment.txt").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken
        {
            get
            {
                var result = POSTMethod("EZPayCreatesOneTimePaymentImmediatePaymentExpiredToken", "/ezpay/onetimeimmediatepayment", @"\Test Data\EZPay\EZPayOneTimeImmediatePayment.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayCreatesOneTimePaymentImmediatePaymentNullCCID
        {
            get
            {
                var result = POSTMethod("EZPayCreatesOneTimePaymentImmediatePaymentNullCCID", "/ezpay/onetimeimmediatepayment", @"\Test Data\EZPay\EZPayOneTimeImmediatePaymentNullCCID.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken
        {
            get
            {
                var result = POSTMethod("EZPayPostOneEarlyPayoutForHPPSessionTokenWithAccessToken", "/ezpay/earlypayout", @"\Test Data\EZPay\EZPayOneTimeEarlyPayout.txt", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken
        {
            get
            {
                var result = POSTMethod("EZPayPostOneEarlyPayoutForHPPSessionTokenWithNoToken", "/ezpay/earlypayout", @"\Test Data\EZPay\EZPayOneTimeEarlyPayout.txt").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken
        {
            get
            {
                var result = POSTMethod("EZPayPostOneEarlyPayoutForHPPSessionTokenWithExpiredToken", "/ezpay/earlypayout", @"\Test Data\EZPay\EZPayOneTimeEarlyPayout.txt", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayEarlyPayoutDetailsWithAccessToken
        {
            get
            {
                var result = GETMethod("EZPayEarlyPayoutDetailsWithAccessToken", "/ezpay/earlypayoutdetails/?agreementId=123&sourceStore=DEMO4", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayEarlyPayoutDetailsWithNoToken
        {
            get
            {
                var result = GETMethod("EZPayEarlyPayoutDetailsWithNoToken", "/ezpay/earlypayoutdetails/?agreementId=123&sourceStore=DEMO4").Execute();

                return result;
            }
        }

        public static IApiBuilderStats EZPayEarlyPayoutDetailsWithExpiredToken
        {
            get
            {
                var result = GETMethod("EZPayEarlyPayoutDetailsWithExpiredToken", "/ezpay/earlypayoutdetails/?agreementId=123&sourceStore=DEMO4", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats POSTMethod(string Name, string resource, string testData, string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.POST)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("EZ_Pay"))
                .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .AddParameter("application/json", File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + testData), ParameterType.RequestBody)
                .SendAs(DataFormat.Json);

            return result;
        }

        public static IApiBuilderStats GETMethod(string Name, string resource, string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("EZ_Pay"))
                .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .SendAs(DataFormat.Json);

            return result;
        }

        public static IApiBuilderStats DELETEMethod(string Name, string resource, string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.DELETE)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("EZ_Pay"))
                .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .SendAs(DataFormat.Json);

            return result;
        }
    }
}