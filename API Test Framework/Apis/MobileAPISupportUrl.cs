﻿using AutoFrameWork.API;
using RestSharp;

namespace API_Test_Framework.Apis
{
    public class MobileAPISupportUrl
    {
        private static GetAuth0LoginResponse authOB = new GetAuth0LoginResponse();
        private static EnvtConfigurator envt = new EnvtConfigurator();

        private static ApiBuilder _api;

        static MobileAPISupportUrl()
        {
            _api = new ApiBuilder();
        }

        private static string accessToken { get; set; }

        public string GetAccessToken()
        {
            string userToken = authOB.GetAccessToken();

            accessToken = userToken;

            return accessToken;
        }

        public static IApiBuilderStats SupportGetTipsWithToken
        {
            get
            {
                var result = GETMethod("SupportGetTipsWithToken", "/Tips/GetTipsRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportGetTipsWithoutToken
        {
            get
            {
                var result = GETMethod("SupportGetTipsWithoutToken", "/Tips/GetTipsRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportGetTipsExpiredToken
        {
            get
            {
                var result = GETMethod("SupportGetTipsExpiredToken", "/Tips/GetTipsRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportGetFAQsWithToken
        {
            get
            {
                var result = GETMethod("SupportGetFAQsWithToken", "/FAQ/GetFAQsRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportFAQsWithoutToken
        {
            get
            {
                var result = GETMethod("SupportGetFAQsWithToken", "/FAQ/GetFAQsRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportFAQsExpiredToken
        {
            get
            {
                var result = GETMethod("SupportFAQsExpiredToken", "/FAQ/GetFAQsRequest?format=json", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportContactWithToken
        {
            get
            {
                var result = GETMethod("SupportContactWithToken", "/Contact/GetContactRequest?format=json&useStub=true&stubParams=agreementCount:3;useRealStoreList:true", "7e8cf26303ad4d2da950ca63b36a67f5", accessToken).Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportContactWithoutToken
        {
            get
            {
                var result = GETMethod("SupportContactWithoutToken", "/Contact/GetContactRequest?format=json&useStub=true&stubParams=agreementCount:5;useRealStoreList:true", "7e8cf26303ad4d2da950ca63b36a67f5").Execute();

                return result;
            }
        }

        public static IApiBuilderStats SupportContactExpiredToken
        {
            get
            {
                var result = GETMethod("SupportContactExpiredToken", "/Contact/GetContactRequest?format=json&useStub=true&stubParams=agreementCount:5;useRealStoreList:true", "7e8cf26303ad4d2da950ca63b36a67f5", "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjE1MCwiaXNzIjoiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVhZGEyM2JmNWZiM2JjMWI2ZDdiMDhhMyIsImF1ZCI6WyJodHRwczovL2FwaS5hYXJvbnMuY29tIiwiaHR0cHM6Ly9hYXJvbnMuYXV0aDAuY29tL3VzZXJpbmZvIl0sImlhdCI6MTUyNDQxODgyOCwiZXhwIjoxNTI0NTA1MjI4LCJhenAiOiJva1Z3UDdrWTNET2xvdjNodWw5eUVkOG5UaGFSTlpGSyIsInNjb3BlIjoib3BlbmlkIGVtYWlsIHJlYWQ6ZXpwYXkgcmVhZDpob21lIHJlYWQ6YWNjb3VudCBvZmZsaW5lX2FjY2VzcyIsImd0eSI6InBhc3N3b3JkIn0.R3Ej-XCMubnL8teANnLSE2Ho57puDzHMiV6vlGJ-JSUL5BVHHF04xmx2TzKZ8rx2BLOcKytC8fmRRkaF8J0KQaaDiLiqYTiOP057iB1SKjPM1k6H6sRiH8s8YxzCJwkeRraTOZy6V9a7qbQj0QYKlmrFVZbHMN2l0-kigoFa7_HOKtSXpE8UkCki37fK9Qy975HzJeGa9DUqneruZkjcFmEoNLRVQhsHGj5-SdAdIGpn1kdW3IcDAm-JSh2KYtugxgT5g_Rkz7mbj3FwCrQ6pU6B8UbF-HecRWmZ_gh80ucJfDTGUQbp8gbbQcfxd_t2egaLwMY0s0Bi8ak0PivNXg").Execute();

                return result;
            }
        }

        public static IApiBuilderStats GETMethod(string Name, string resource, string subscriptionNumber, string bearerToken = "")
        {
            var result = _api.Name(Name)
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(envt.envtSetup("Support"))
                      .Resource(resource)
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .AddHeader("Authorization", "Bearer " + bearerToken)
                .AddHeader("Ocp-Apim-Subscription-Key", subscriptionNumber)
                .SendAs(DataFormat.Json);

            return result;
        }
    }
}