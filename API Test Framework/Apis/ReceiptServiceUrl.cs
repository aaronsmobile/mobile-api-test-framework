﻿using AutoFrameWork.API;
using AutoFrameWork.Core;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace API_Test_Framework.Apis
{
    public class ReceiptServiceUrl
    {
        private static ApiBuilder _api;

        static ReceiptServiceUrl()
        {
            _api = new ApiBuilder();
        }

        public static IApiBuilderStats ReceiptServiceGetSmallReceipt =>
            _api.Name("Get Small Reciept")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.ReceiptServiceQAUrl.SupplyValue())
                .Resource("/getreceipt/SMALL/500569")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats ReceiptServiceGetLargeReceipt =>
            _api.Name("Get Large Receipt")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.ReceiptServiceQAUrl.SupplyValue())
                .Resource("/getreceipt/LARGE/500569")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .SendAs(DataFormat.Json)
                .Execute();

        public static IApiBuilderStats ReceiptServiceGetSpecificReceipt =>
            _api.Name("Get Specific Receipt")
                .Use(Method.GET)
                .ReturnAs(DataFormat.Json)
                .UseUrl(Settings.Setting.NonProdEnvironments.ReceiptServiceQAUrl.SupplyValue())
                .Resource("getreceipt/F284/500569")
                .AddHeader("Accept", "application/json")
                .AddHeader("Content-Type", "application/json")
                .SendAs(DataFormat.Json)
                .Execute();

    }

}
