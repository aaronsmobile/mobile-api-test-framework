﻿using System;

namespace API_Test_Framework.Settings
{
    internal class Attributes
    {
        // <summary>
        /// Attribute.
        /// </summary>
        [AttributeUsage(AttributeTargets.Class)]
        public class TribeAttribute : Attribute
        {
            /// <summary>
            /// Stores string field.
            /// </summary>
            private string _tribe;

            /// <summary>
            /// Attribute constructor.
            /// </summary>
            public TribeAttribute(string tribe)
            {
                this._tribe = tribe;
            }

            /// <summary>
            /// Get Id.
            /// </summary>
            public string TribeInfo
            {
                get { return this._tribe; }
            }
        }
    }
}