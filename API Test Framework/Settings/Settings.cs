﻿using System.ComponentModel;

namespace API_Test_Framework.Settings
{
    public class Setting
    {
        public enum ProdEnvironments
        {
            [Description("https://mobileapisupport.aarons.com")]
            MobileAPISupportProdUrl,

            [Description("https://mobileapiaccount.aarons.com")]
            MobileAPIAccountProdUrl,

            [Description("https://clubmembershipservice.aarons.com/")]
            ClubMembershipServiceProdUrl,

            [Description("https://mobileapiezpay.aarons.com/")]
            MobileAPIEZPayProdUrl,

            [Description("https://mobileapihome.aarons.com/")]
            MobileAPIHomeProdUrl,

            [Description("https://mobileapionboarding.aarons.com")]
            MobileAPIOnBoardingProdUrl,

            [Description("http://receiptservice.aarons.com")]
            ReceiptServiceProdUrl
        }

        public enum NonProdEnvironments
        {
            [Description("https://aarons.auth0.com/")]
            AaronsAuth0Url,

            [Description("https://dev-869245.oktapreview.com/")]
            OktaUrl,

            [Description("https://mobileapisupport-np.aarons.com")]
            MobileAPISupportUrl,

            [Description("https://mobileapiaccount-np.aarons.com")]
            MobileAPIAccountUrl,

            [Description("https://clubmembershipserviceqa.aarons.com/")]
            ClubMembershipServiceUrl,

            [Description("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik9UaEZNVFl3UVVWQ01ESXlRemsxT1VZeU16UXdRVVUwTVRsQlJEVTFOVUpET0RNME9FTkdNQSJ9.eyJodHRwczovL2Fhcm9uc2N1c3RvbWVyL2dsb2JhbGN1c3RvbWVyaWQiOjUwLCJpc3MiOiJodHRwczovL2Fhcm9ucy5hdXRoMC5jb20vIiwic3ViIjoiYXV0aDB8NWFkYTIzYmY1ZmIzYmMxYjZkN2IwOGEzIiwiYXVkIjpbImh0dHBzOi8vYXBpLmFhcm9ucy5jb20iLCJodHRwczovL2Fhcm9ucy5hdXRoMC5jb20vdXNlcmluZm8iXSwiaWF0IjoxNTI1NjQwMjA1LCJleHAiOjE1MjU3MjY2MDUsImF6cCI6Im9rVndQN2tZM0RPbG92M2h1bDl5RWQ4blRoYVJOWkZLIiwic2NvcGUiOiJvcGVuaWQgZW1haWwgcmVhZDplenBheSByZWFkOmhvbWUgcmVhZDphY2NvdW50IG9mZmxpbmVfYWNjZXNzIiwiZ3R5IjoicGFzc3dvcmQifQ.BVfnsDx2io147SEpzLvuj_jjzleFB3zmphdJcPpEth_H10rDuI37-U7asG8jkq1UpdiiNsFQILJV4T97nCgg-Yv6W4jUll1QnI_NYC29DqHcvdnuk-1zsxvGKZ5OX--QtZGyOUH5Z_CDQzPIThBRxxOKjwdu6W86EfO-Yqs5iRPK_D-UiAeHjfFW8_5MwqBaqWjpv-gjalkmuWNgbTCffjueHEbvdtD0Sv1rf9jcCt30kYzHAEmQxAZbh9OhKJRqyWm4Q6GPtEaMA0OKVRlucdiTel9Qg-eZa2a86pkWl_mgFpaUnK6Vvrz5n-ghp43jw2ogXfPyzuEJ2RWFvHwJYw")]
            AccessBearerToken,

            [Description("https://mobileapiezpay-np.aarons.com/")]
            MobileAPIEZPayUrl,

            [Description("https://mobileapihome-np.aarons.com/")]
            MobileAPIHomeUrl,

            [Description("https://mobileapionboarding-np.aarons.com")]
            MobileAPIOnBoardingUrl,

            [Description("http://receiptservicedev.aarons.com")]
            ReceiptServiceDevUrl,

            [Description("http://receiptserviceqa.aarons.com")]
            ReceiptServiceQAUrl,

            [Description("https://mobileapisupport-staging.aarons.com")]
            MobileAPISupportStagingUrl,

            [Description("https://mobileapiaccount-staging.aarons.com")]
            MobileAPIAccountStagingUrl,

            [Description("http://mobileapiezpay-staging.aarons.com/")]
            MobileAPIEZPayStagingUrl,

            [Description("https://mobileapihome-staging.aarons.com/")]
            MobileAPIHomeStagingUrl,

            [Description("https://mobileapionboarding-staging.aarons.com")]
            MobileAPIOnBoardingStagingUrl,
        }

        public enum StagingEnvironments
        {
            [Description("https://mobileapisupport-staging.aarons.com")]
            MobileAPISupportStagingUrl,

            [Description("https://mobileapiaccount-staging.aarons.com")]
            MobileAPIAccountStagingUrl,

            [Description("http://mobileapiezpay-staging.aarons.com/")]
            MobileAPIEZPayStagingUrl,

            [Description("https://mobileapihome-staging.aarons.com/")]
            MobileAPIHomeStagingUrl,

            [Description("https://mobileapionboarding-staging.aarons.com")]
            MobileAPIOnBoardingStagingUrl,
        }
    }
}